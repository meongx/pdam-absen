﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Ribbon;
using DevExpress.Xpo;
using DevExpress.Xpf.Printing;
using DevExpress.Data.Filtering;

namespace PDAM.Absen
{
    public partial class MainWindow : Window
    {
        private Dictionary<String,UserControl> pages = new Dictionary<String,UserControl>();

        private XPCollection dataShiftSchedule;
        private XPCollection dataRegularSchedule;
        private XPCollection dataFreeDay;
        //private XPServerCollectionSource dataPegawai;

        public MainWindow()
        {
            InitializeComponent();

        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            if (dataShiftSchedule == null) dataShiftSchedule = new XPCollection(typeof(Data.ShiftSchedule));
            if (!pages.ContainsKey("ShiftView")) pages.Add("ShiftView", new Pages.ShiftSched(dataShiftSchedule));

            mainContainer.Child = pages["ShiftView"];

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (dataShiftSchedule == null) dataShiftSchedule = new XPCollection(typeof(Data.ShiftSchedule));
            new Dialogs.ShiftSchedEditor(dataShiftSchedule).Show();
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            new Dialogs.Location().Show();
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            //if (dataPegawai == null) dataPegawai = new XPServerCollectionSource(XpoDefault.Session,typeof(Data.Pegawai));
            if (!pages.ContainsKey("PegawaiView")) pages.Add("PegawaiView", new Pages.Pegawai());

            mainContainer.Child = pages["PegawaiView"];

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            new Dialogs.AssignSchedule().Show();

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            if (dataRegularSchedule == null) dataRegularSchedule = new XPCollection(typeof(Data.RegularSchedule));
            if (!pages.ContainsKey("RegularView")) pages.Add("RegularView",new Pages.RegSched(dataRegularSchedule));

            mainContainer.Child = pages["RegularView"];

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            if (dataRegularSchedule == null) dataRegularSchedule = new XPCollection(typeof(Data.RegularSchedule));
            new Dialogs.RegularSchedEditor(dataRegularSchedule).Show();

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Cursor = Cursors.Wait;

            if (dataFreeDay == null) dataFreeDay = new XPCollection(typeof(Data.FreeDay));
            if (!pages.ContainsKey("FreeDayView")) pages.Add("FreeDayView", new Pages.FreeDay(dataFreeDay));

            mainContainer.Child = pages["FreeDayView"];

            this.Cursor = Cursors.Arrow;
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;

            if (dataFreeDay == null) dataFreeDay = new XPCollection(typeof(Data.FreeDay));
            new Dialogs.FreeDayEditor(dataFreeDay).Show();

            Cursor = Cursors.Arrow;
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            new Dialogs.GenerateSchedule().Show();
        }

        private void barButtonItem12_ItemClick(object sender, ItemClickEventArgs e)
        {
            new Dialogs.ProcessLog().Show();
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;
            new Report.PeroranganPreview().Show();
            Cursor = Cursors.Arrow;
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;
            new Dialogs.ManualPresence().Show();
            Cursor = Cursors.Arrow;
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            new Dialogs.ManualPermission().Show();
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;
            new Dialogs.SwapSchedule().Show();
            Cursor = Cursors.Arrow;
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;
            new Report.DivisiPreview().Show();
            Cursor = Cursors.Arrow;
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            new Dialogs.ImportLog().Show();
        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor = Cursors.Wait;
            new Dialogs.ApplyAbsen().Show();
            Cursor = Cursors.Arrow;
        }
    }


}
