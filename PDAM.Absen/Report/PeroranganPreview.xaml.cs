﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Printing;
using DevExpress.Xpo;

namespace PDAM.Absen.Report
{
    /// <summary>
    /// Interaction logic for PeroranganPreview.xaml
    /// </summary>
    public partial class PeroranganPreview : Window
    {
        private Perorangan report = new Perorangan();

        public PeroranganPreview()
        {
            InitializeComponent();
            
            report.DataSource = new XPCollection(typeof(Data.Absen));
            
            var model = new XtraReportPreviewModel(report);
            documentPreview1.Model = model;

            lookUpEdit1.ItemsSource = Data.Pegawai.GetDataView();

            var now = DateTime.Today;
            if (now.Day < 16)
            {
                dateEdit1.EditValue = new DateTime((now.Month == 1 ? now.Year - 1 : now.Year), (now.Month == 1 ? 12 : now.Month - 1), 16);
                dateEdit2.EditValue = new DateTime(now.Year, now.Month, 15);
            }
            else
            {
                dateEdit1.EditValue = new DateTime(now.Year, now.Month, 16);
                dateEdit2.EditValue = new DateTime((now.Month == 12 ? now.Year + 1 : now.Year), (now.Month == 12 ? 1 : now.Month + 1), 15);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var st = dateEdit1.EditValue as DateTime?;
            var et = dateEdit2.EditValue as DateTime?;
            var peg = lookUpEdit1.EditValue as Data.Pegawai;

            if (st == null || et == null || peg == null) return;

            report.StartDate.Value = st.Value;
            report.EndDate.Value = et.Value;
            report.Nip.Value = peg.Nip;
            report.CreateDocument();
        }
    }
}
