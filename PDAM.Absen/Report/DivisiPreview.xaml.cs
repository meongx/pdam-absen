﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using DevExpress.Xpo;
using DevExpress.Xpf.Printing;

namespace PDAM.Absen.Report
{
    /// <summary>
    /// Interaction logic for DivisiPreview.xaml
    /// </summary>
    public partial class DivisiPreview : Window
    {
        //private Divisi report = new Divisi();
        private DivisiWithPelanggaran report = new DivisiWithPelanggaran();

        public DivisiPreview()
        {
            InitializeComponent();

            //var view = new XPView(XpoDefault.Session, typeof(Data.PegawaiView));
            //view.Properties.Add(new ViewProperty("KodeBagian", SortDirection.Ascending, "[KodeBagian]", true, true));
            //view.Properties.Add(new ViewProperty("NamaBagian",SortDirection.Ascending,"[NamaBagian]",true,true));
            //comboBoxEdit1.ItemsSource = view;
            comboBoxEdit1.ItemsSource = new XPCollection<Data.Bagian>();

            var model = new XtraReportPreviewModel(report);
            documentPreview1.Model = model;

            var now = DateTime.Today;
            //if (now.Day < 16)
            //{
            //    dateEdit1.EditValue = new DateTime((now.Month == 1 ? now.Year - 1 : now.Year), (now.Month == 1 ? 12 : now.Month - 1), 16);
            //    dateEdit2.EditValue = new DateTime(now.Year, now.Month, 15);
            //}
            //else
            //{
            //    dateEdit1.EditValue = new DateTime(now.Year, now.Month, 16);
            //    dateEdit2.EditValue = new DateTime((now.Month == 12 ? now.Year + 1 : now.Year), (now.Month == 12 ? 1 : now.Month + 1), 15);
            //}
            dateEdit1.EditValue = new DateTime((now.Month == 1 ? now.Year - 1 : now.Year), (now.Month == 1 ? 12 : now.Month - 1), 16);
            dateEdit2.EditValue = new DateTime(now.Year, now.Month, 15);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var st = dateEdit1.EditValue as DateTime?;
            var et = dateEdit2.EditValue as DateTime?;
            var bag = comboBoxEdit1.EditValue as string;

            if (st == null || et == null || String.IsNullOrEmpty(bag)) return;

            if (bag.Substring(bag.Length - 1, 1) == "0")
                bag = bag.Substring(0, bag.Length - 1) + "%";

            report.StartDate.Value = st.Value;
            report.EndDate.Value = et.Value;
            report.KodeBagian.Value = bag;
            report.NamaBagian.Value = comboBoxEdit1.Text;
            report.CreateDocument();
        }
    }

    public class IsSubBagConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var s = value as string;
            return (s.Substring(s.Length - 1, 1) != "0" && s.Substring(0,2) != "0P" && s.Substring(0,2) != "A0");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException("IsSubBagConverter can only be used OneWay.");
        }
    }
}
