﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace PDAM.Absen.Report
{
    public partial class Perorangan : DevExpress.XtraReports.UI.XtraReport
    {
        private TimeSpan totalHours;
        private TimeSpan terlambat;
        private TimeSpan izinTerlambat;
        private TimeSpan pulangCepat;
        private TimeSpan izinPulang;
        private int mangkir;
        private int izinAbsen;

        public Perorangan()
        {
            InitializeComponent();
        }

        private void xrLabel9_SummaryReset(object sender, EventArgs e)
        {
            totalHours = TimeSpan.Zero;
        }

        private void xrLabel9_SummaryRowChanged(object sender, EventArgs e)
        {
            var ts = GetCurrentColumnValue("TotalWorkHour") as TimeSpan?;

            if (ts != null)
                totalHours += ts.Value;
        }

        private void xrLabel9_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(totalHours);
            e.Handled = true;
        }

        private void xrLabel17_SummaryReset(object sender, EventArgs e)
        {
            terlambat = TimeSpan.Zero;
        }

        private void xrLabel17_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.Terlambat || izin == Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = GetCurrentColumnValue("Terlambat") as TimeSpan?;
            terlambat += ts.Value;
        }

        private void xrLabel17_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(terlambat);
            e.Handled = true;
        }

        private void xrLabel18_SummaryReset(object sender, EventArgs e)
        {
            izinTerlambat = TimeSpan.Zero;
        }

        private void xrLabel18_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin != Data.AbsenIzin.Terlambat && izin != Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = GetCurrentColumnValue("Terlambat") as TimeSpan?;
            izinTerlambat += ts.Value;
        }

        private void xrLabel18_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(izinTerlambat);
            e.Handled = true;
        }

        private string GetResult(TimeSpan field)
        {
            return String.Format("{0:00} jam {1:00} menit", field.Days * 24 + field.Hours, field.Minutes);
        }

        private void xrLabel19_SummaryReset(object sender, EventArgs e)
        {
            pulangCepat = TimeSpan.Zero;
        }

        private void xrLabel19_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.PulangCepat || izin == Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = GetCurrentColumnValue("PulangCepat") as TimeSpan?;
            pulangCepat += ts.Value;
        }

        private void xrLabel19_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(pulangCepat);
            e.Handled = true;
        }

        private void xrLabel20_SummaryReset(object sender, EventArgs e)
        {
            izinPulang = TimeSpan.Zero;
        }

        private void xrLabel20_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin != Data.AbsenIzin.PulangCepat && izin != Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = GetCurrentColumnValue("PulangCepat") as TimeSpan?;
            izinPulang += ts.Value;
        }

        private void xrLabel20_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(izinPulang);
            e.Handled = true;
        }

        private void xrLabel22_SummaryReset(object sender, EventArgs e)
        {
            mangkir = 0;
        }

        private void xrLabel22_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.TidakMasuk) return;

            var c1 = GetCurrentColumnValue("CheckInTime") as DateTime?;
            var c2 = GetCurrentColumnValue("CheckOutTime") as DateTime?;

            if ((c1 == null || c1.Value.Year == 1) && (c2 == null || c2.Value.Year == 1))
                mangkir++;
        }

        private void xrLabel22_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = mangkir + " hari";
            e.Handled = true;
        }

        private void xrLabel21_SummaryReset(object sender, EventArgs e)
        {
            izinAbsen = 0;
        }

        private void xrLabel21_SummaryRowChanged(object sender, EventArgs e)
        {
            var izin = GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin != Data.AbsenIzin.TidakMasuk) return;

            izinAbsen++;
        }

        private void xrLabel21_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = izinAbsen + " hari";
            e.Handled = true;
        }

        private void xrLabel23_SummaryCalculated(object sender, TextFormatEventArgs e)
        {
            if (e.Value != null)
                e.Text = GetResult((TimeSpan)e.Value);
        }

    }
}
