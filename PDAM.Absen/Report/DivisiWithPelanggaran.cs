﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace PDAM.Absen.Report
{
    public partial class DivisiWithPelanggaran : DevExpress.XtraReports.UI.XtraReport
    {
        private TimeSpan totalHours;
        private TimeSpan terlambat;
        private TimeSpan izinTerlambat;
        private TimeSpan pulangCepat;
        private TimeSpan izinPulang;
        private int mangkir;
        private int izinAbsen;

        public DivisiWithPelanggaran()
        {
            InitializeComponent();

            xrTotalHours.SummaryReset += new EventHandler(xrTotalHours_SummaryReset);
            xrTotalHours.SummaryRowChanged += new EventHandler(xrTotalHours_SummaryRowChanged);
            xrTotalHours.SummaryGetResult += new SummaryGetResultHandler(xrTotalHours_SummaryGetResult);

            xrTerlambat.SummaryGetResult += new SummaryGetResultHandler(xrTerlambat_SummaryGetResult);
            xrTerlambat.SummaryRowChanged += new EventHandler(xrTerlambat_SummaryRowChanged);
            xrTerlambat.SummaryReset += new EventHandler(xrTerlambat_SummaryReset);

            xrIzinTerlambat.SummaryGetResult += new SummaryGetResultHandler(xrIzinTerlambat_SummaryGetResult);
            xrIzinTerlambat.SummaryRowChanged += new EventHandler(xrIzinTerlambat_SummaryRowChanged);
            xrIzinTerlambat.SummaryReset += new EventHandler(xrIzinTerlambat_SummaryReset);

            xrPulangCepat.SummaryGetResult += new SummaryGetResultHandler(xrPulangCepat_SummaryGetResult);
            xrPulangCepat.SummaryRowChanged += new EventHandler(xrPulangCepat_SummaryRowChanged);
            xrPulangCepat.SummaryReset += new EventHandler(xrPulangCepat_SummaryReset);

            xrIzinPulang.SummaryGetResult += new SummaryGetResultHandler(xrIzinPulang_SummaryGetResult);
            xrIzinPulang.SummaryRowChanged += new EventHandler(xrIzinPulang_SummaryRowChanged);
            xrIzinPulang.SummaryReset += new EventHandler(xrIzinPulang_SummaryReset);

            xrMangkir.SummaryGetResult += new SummaryGetResultHandler(xrMangkir_SummaryGetResult);
            xrMangkir.SummaryRowChanged += new EventHandler(xrMangkir_SummaryRowChanged);
            xrMangkir.SummaryReset += new EventHandler(xrMangkir_SummaryReset);

            xrIzinTidakMasuk.SummaryGetResult += new SummaryGetResultHandler(xrIzinTidakMasuk_SummaryGetResult);
            xrIzinTidakMasuk.SummaryRowChanged += new EventHandler(xrIzinTidakMasuk_SummaryRowChanged);
            xrIzinTidakMasuk.SummaryReset += new EventHandler(xrIzinTidakMasuk_SummaryReset);
        }

        void xrIzinTidakMasuk_SummaryReset(object sender, EventArgs e)
        {
            izinAbsen = 0;
        }

        void xrIzinTidakMasuk_SummaryRowChanged(object sender, EventArgs e)
        {
            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin.Value != Data.AbsenIzin.TidakMasuk) return;

            izinAbsen++;
        }

        void xrIzinTidakMasuk_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = izinAbsen + " hari";
            e.Handled = true;
        }

        void xrMangkir_SummaryReset(object sender, EventArgs e)
        {
            mangkir = 0;
        }

        void xrMangkir_SummaryRowChanged(object sender, EventArgs e)
        {
            //var dayoff = (bool)GetCurrentColumnValue("Absens.IsDayOff");
            var dayoff = (bool)DetailReport.GetCurrentColumnValue("IsDayOff");
            if (dayoff) return;

            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.TidakMasuk) return;

            var c1 = DetailReport.GetCurrentColumnValue("CheckInTime") as DateTime?;
            var c2 = DetailReport.GetCurrentColumnValue("CheckOutTime") as DateTime?;

            if ((c1 == null || c1.Value.Year == 1) && (c2 == null || c2.Value.Year == 1))
                mangkir++;
        }

        void xrMangkir_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = mangkir + " hari";
            e.Handled = true;
        }

        void xrIzinPulang_SummaryReset(object sender, EventArgs e)
        {
            izinPulang = TimeSpan.Zero;
        }

        void xrIzinPulang_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = (bool)DetailReport.GetCurrentColumnValue("IsDayOff");
            if (dayoff) return;

            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin != Data.AbsenIzin.PulangCepat && izin != Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = DetailReport.GetCurrentColumnValue("PulangCepat") as TimeSpan?;
            izinPulang += ts.Value;
        }

        void xrIzinPulang_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(izinPulang);
            e.Handled = true;
        }

        void xrPulangCepat_SummaryReset(object sender, EventArgs e)
        {
            pulangCepat = TimeSpan.Zero;
        }

        void xrPulangCepat_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = (bool)DetailReport.GetCurrentColumnValue("IsDayOff");
            if (dayoff) return;

            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.PulangCepat || izin == Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = DetailReport.GetCurrentColumnValue("PulangCepat") as TimeSpan?;
            pulangCepat += ts.Value;
        }

        void xrPulangCepat_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(pulangCepat);
            e.Handled = true;
        }

        void xrIzinTerlambat_SummaryReset(object sender, EventArgs e)
        {
            izinTerlambat = TimeSpan.Zero;
        }

        void xrIzinTerlambat_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = DetailReport.GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin != Data.AbsenIzin.Terlambat && izin != Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = DetailReport.GetCurrentColumnValue("Terlambat") as TimeSpan?;
            izinTerlambat += ts.Value;
        }

        void xrIzinTerlambat_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(izinTerlambat);
            e.Handled = true;
        }

        void xrTerlambat_SummaryReset(object sender, EventArgs e)
        {
            terlambat = TimeSpan.Zero;
        }

        void xrTerlambat_SummaryRowChanged(object sender, EventArgs e)
        {
            var dayoff = DetailReport.GetCurrentColumnValue("IsDayOff") as bool?;
            if (dayoff.Value) return;

            var izin = DetailReport.GetCurrentColumnValue("Izin") as Data.AbsenIzin?;
            if (izin == Data.AbsenIzin.Terlambat || izin == Data.AbsenIzin.TerlambatDanPulangCepat) return;

            var ts = DetailReport.GetCurrentColumnValue("Terlambat") as TimeSpan?;
            terlambat += ts.Value;
        }

        void xrTerlambat_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(terlambat);
            e.Handled = true;
        }

        void xrTotalHours_SummaryGetResult(object sender, SummaryGetResultEventArgs e)
        {
            e.Result = GetResult(totalHours);
            e.Handled = true;
        }

        void xrTotalHours_SummaryRowChanged(object sender, EventArgs e)
        {
            var ts = DetailReport.GetCurrentColumnValue("TotalWorkHour") as TimeSpan?;

            if (ts != null)
                totalHours += ts.Value;
        }

        void xrTotalHours_SummaryReset(object sender, EventArgs e)
        {
            totalHours = TimeSpan.Zero;
        }

        private string GetResult(TimeSpan field)
        {
            return String.Format("{0:00} jam {1:00} menit", field.Days * 24 + field.Hours, field.Minutes);
        }

        private void xrLabel18_SummaryCalculated(object sender, TextFormatEventArgs e)
        {
            if (e.Value != null)
                e.Text = GetResult((TimeSpan)e.Value);
        }
    }
}
