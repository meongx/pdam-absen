﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;

namespace PDAM.Absen
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Height = 60;

            tHost.Text = Properties.Settings.Default.MySqlHost;
            tUser.Text = Properties.Settings.Default.MySqlUser;
            tPass.Text = Properties.Settings.Default.MySqlPass;
            tDB.Text = Properties.Settings.Default.MySqlDBName;

            if (Connect())
            {
                (new MainWindow()).Show();
                Close();
            }
            else
            {
                Height = 200;
            }
        }

        private bool Connect()
        {
            Cursor = Cursors.Wait;
            tStatus.Text = "Connecting to MySQL Server";

            try
            {
                XpoDefault.DataLayer = XpoDefault.GetDataLayer(
                        MySqlConnectionProvider.GetConnectionString(tHost.Text, tUser.Text, tPass.Text, tDB.Text),
                        AutoCreateOption.DatabaseAndSchema
                    );
                XpoDefault.Session.LockingOption = LockingOption.None;
            }
            catch
            {
                tStatus.Text = "Unable to connect to MySQL Server";
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

            return true;
        }

        private void bConnect_Click(object sender, RoutedEventArgs e)
        {
            if (Connect())
            {
                Properties.Settings.Default.MySqlHost = tHost.Text;
                Properties.Settings.Default.MySqlUser = tUser.Text;
                Properties.Settings.Default.MySqlPass = tPass.Text;
                Properties.Settings.Default.MySqlDBName = tDB.Text;

                Properties.Settings.Default.Save();

                (new MainWindow()).Show();
                Close();
            }
        }
    }
}
