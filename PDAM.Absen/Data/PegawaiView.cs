using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("pegawai_lookup")]
    public class PegawaiView : XPLiteObject
    {
        public PegawaiView()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public PegawaiView(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private string _NamaJabatan;
        private int _KodeJabatan;
        private string _NamaBagian;
        private string _KodeBagian;
        private string _Nama;
        private string _Nip;

        [Persistent("nip"),Key(false)]
        public string Nip
        {
            get
            {
                return _Nip;
            }
            set
            {
                SetPropertyValue("Nip", ref _Nip, value);
            }
        }

        [Persistent("nama")]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                SetPropertyValue("Nama", ref _Nama, value);
            }
        }

        [Persistent("kodebag")]
        public string KodeBagian
        {
            get
            {
                return _KodeBagian;
            }
            set
            {
                SetPropertyValue("KodeBagian", ref _KodeBagian, value);
            }
        }

        [Persistent("namabag")]
        public string NamaBagian
        {
            get
            {
                return _NamaBagian;
            }
            set
            {
                SetPropertyValue("NamaBagian", ref _NamaBagian, value);
            }
        }

        [Persistent("kodejab")]
        public int KodeJabatan
        {
            get
            {
                return _KodeJabatan;
            }
            set
            {
                SetPropertyValue("KodeJabatan", ref _KodeJabatan, value);
            }
        }

        [Persistent("namajab")]
        public string NamaJabatan
        {
            get
            {
                return _NamaJabatan;
            }
            set
            {
                SetPropertyValue("NamaJabatan", ref _NamaJabatan, value);
            }
        }

    }

}