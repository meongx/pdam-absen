using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_ShiftSched")]
    public class ShiftSchedule : XPObject
    {
        public ShiftSchedule()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public ShiftSchedule(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        // Fields...
        private string _Name;

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }

        [Association("ShiftSchedule-TimeShifts"),Aggregated]
        public XPCollection<TimeShift> TimeShifts
        {
            get
            {
                return GetCollection<TimeShift>("TimeShifts");
            }
        }
    }

}