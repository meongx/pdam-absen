using System;
using System.Collections.Generic;
using DevExpress.Xpo;
using System.Linq;

namespace PDAM.Absen.Data
{
    //[Persistent("absen_Pegawai_phantom")]
    [NonPersistent]
    public class Pegawai : XPLiteObject
    {
        public Pegawai()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Pegawai(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        public static ICollection<Pegawai> GetDataView()
        {
            var querystring = "SELECT nip,nama,kodebag,namabag,kodejab,namajab,OID,NextDayOrder,ShiftSchedule,RegularSchedule FROM pegawai_lookup LEFT JOIN absen_Pegawai ON pegawai_lookup.nip = absen_pegawai.Link";

            if (XpoDefault.Session.UpdateSchema(true, new DevExpress.Xpo.Metadata.XPClassInfo[] { XpoDefault.Session.GetClassInfo<Data.PegawaiLink>() }) == DevExpress.Xpo.DB.UpdateSchemaResult.FirstTableNotExists)
            {
                XpoDefault.Session.UpdateSchema(typeof(Data.PegawaiLink));
            }
            return XpoDefault.Session.GetObjectsFromQuery<Pegawai>(querystring);
        }

        // Fields...
        private RegularSchedule _RegularSchedule;
        private int _Oid;
        private string _NamaJabatan;
        private string _KodeJabatan;
        private string _NamaBagian;
        private string _KodeBagian;
        private string _Nama;
        private string _Nip;
        private ShiftSchedule _ShiftSched;
        private int _NextDayOrder;

        [Persistent("nip"), Key(false)]
        public string Nip
        {
            get
            {
                return _Nip;
            }
            set
            {
                SetPropertyValue("Nip", ref _Nip, value);
            }
        }

        [Persistent("nama")]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                SetPropertyValue("Nama", ref _Nama, value);
            }
        }

        [Persistent("kodebag")]
        public string KodeBagian
        {
            get
            {
                return _KodeBagian;
            }
            set
            {
                SetPropertyValue("KodeBagian", ref _KodeBagian, value);
            }
        }

        [Persistent("namabag")]
        public string NamaBagian
        {
            get
            {
                return _NamaBagian;
            }
            set
            {
                SetPropertyValue("NamaBagian", ref _NamaBagian, value);
            }
        }

        [Persistent("kodejab")]
        public string KodeJabatan
        {
            get
            {
                return _KodeJabatan;
            }
            set
            {
                SetPropertyValue("KodeJabatan", ref _KodeJabatan, value);
            }
        }

        [Persistent("namajab")]
        public string NamaJabatan
        {
            get
            {
                return _NamaJabatan;
            }
            set
            {
                SetPropertyValue("NamaJabatan", ref _NamaJabatan, value);
            }
        }

        [Persistent("OID")]
        public int Oid
        {
            get
            {
                return _Oid;
            }
            set
            {
                SetPropertyValue("Oid", ref _Oid, value);
            }
        }

        public int NextDayOrder
        {
            get
            {
                return _NextDayOrder;
            }
            set
            {
                SetPropertyValue("NextDayOrder", ref _NextDayOrder, value);
                
                _NextDay = null;
                OnChanged("sNextDay");
            }
        }

        public ShiftSchedule ShiftSchedule
        {
            get
            {
                return _ShiftSched;
            }
            set
            {
                SetPropertyValue("ShiftSchedule", ref _ShiftSched, value);
                OnChanged("Assignment");
            }
        }

        public RegularSchedule RegularSchedule
        {
            get
            {
                return _RegularSchedule;
            }
            set
            {
                SetPropertyValue("RegularSchedule", ref _RegularSchedule, value);
                OnChanged("Assignment");
            }
        }

        [NonPersistent]
        public string Assignment
        {
            get
            {
                if (_ShiftSched != null) return "Shift: " + _ShiftSched.Name;
                if (_RegularSchedule != null) return "Reguler: " + _RegularSchedule.Name;

                return "";
            }
        }

        private PegawaiLink _Link;
        [NonPersistent]
        public PegawaiLink Link
        {
            get
            {
                if (_Link == null) _Link = Session.GetObjectByKey<PegawaiLink>(_Oid);
                return _Link;
            }
            set
            {
                SetPropertyValue("Link", ref _Link, value);
                OnChanged("NextDay"); OnChanged("sNextDay"); OnChanged("Assignment");
            }
        }

        private TimeShift _NextDay;
        [NonPersistent]
        public TimeShift NextDay
        {
            get
            {
                if (_ShiftSched == null) return null;

                if (_ShiftSched.TimeShifts.Sorting.Count < 1) 
                    _ShiftSched.TimeShifts.Sorting.Add(new SortProperty("DayOrder", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_NextDay == null) _NextDay = _ShiftSched.TimeShifts[(_NextDayOrder < _ShiftSched.TimeShifts.Count) ? _NextDayOrder : 0];
                //if (_NextDay == null) _NextDay = _ShiftSched.TimeShifts.First(d => d.DayOrder == _NextDayOrder);
                return _NextDay;
            }
        }

        [NonPersistent]
        public String sNextDay
        {
            get
            {
                if (_ShiftSched == null) return "";

                return string.Format("Hari ke-{0}; {1} ({2:HH:mm}-{3:HH:mm})", NextDay.DayOrder, NextDay.Ket, NextDay.StartTime, NextDay.EndTime);
            }
        }
    }

}