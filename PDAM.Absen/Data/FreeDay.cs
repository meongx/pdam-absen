using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_FreeDay")]
    public class FreeDay : XPObject
    {
        public FreeDay()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public FreeDay(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        // Fields...
        private string _Ket;
        private int _Day;
        private int _Month;
        private int _Year;
        private bool _AffectsRegular;
        private bool _AffectsShift;
        private DateTime _Date;

        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                SetPropertyValue("Ket", ref _Ket, value);
            }
        }

        public int Year
        {
            get
            {
                return _Year;
            }
            set
            {
                SetPropertyValue("Year", ref _Year, value);
            }
        }

        public int Month
        {
            get
            {
                return _Month;
            }
            set
            {
                SetPropertyValue("Month", ref _Month, value);
            }
        }

        public int Day
        {
            get
            {
                return _Day;
            }
            set
            {
                SetPropertyValue("Day", ref _Day, value);
            }
        }

        public bool AffectsShift
        {
            get
            {
                return _AffectsShift;
            }
            set
            {
                SetPropertyValue("AffectsShift", ref _AffectsShift, value);
            }
        }

        public bool AffectsRegular
        {
            get
            {
                return _AffectsRegular;
            }
            set
            {
                SetPropertyValue("AffectsRegular", ref _AffectsRegular, value);
            }
        }

        [NonPersistent]
        public DateTime Date
        {
            get
            {
                if (_Date.Year == 1) _Date = new DateTime((_Year == 0 ? DateTime.Now.Year : _Year), _Month, _Day);
                return _Date;
            }
        }

        [NonPersistent]
        public string DisplayMonth
        {
            get
            {
                return System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat.GetMonthName(_Month);
            }
        }
    }

}