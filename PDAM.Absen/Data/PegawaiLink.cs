using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_Pegawai")]
    [DeferredDeletion(false)]
    public class PegawaiLink : XPObject
    {
        public PegawaiLink()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public PegawaiLink(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private int _Oid;
        private RegularSchedule _RegularSchedule;
        private ShiftSchedule _ShiftSched;
        private string _Link;
        private int _NextDayOrder;

        public int NextDayOrder
        {
            get
            {
                return _NextDayOrder;
            }
            set
            {
                SetPropertyValue("NextDayOrder", ref _NextDayOrder, value);
            }
        }

        public ShiftSchedule ShiftSchedule
        {
            get
            {
                return _ShiftSched;
            }
            set
            {
                SetPropertyValue("ShiftSchedule", ref _ShiftSched, value);
            }
        }

        public RegularSchedule RegularSchedule
        {
            get
            {
                return _RegularSchedule;
            }
            set
            {
                SetPropertyValue("RegularSchedule", ref _RegularSchedule, value);
            }
        }

        public string Link
        {
            get
            {
                return _Link;
            }
            set
            {
                SetPropertyValue("Link", ref _Link, value);
                
                _Pegawai = null;
                OnChanged("Pegawai");
            }
        }

        [Association("PegawaiLink-Absens"),Aggregated]
        public XPCollection<Absen> Absens
        {
            get
            {
                return GetCollection<Absen>("Absens");
            }
        }

        private PegawaiView _Pegawai;
        [NonPersistent]
        public PegawaiView DataPegawai
        {
            get
            {
                if (_Pegawai == null) _Pegawai = Session.GetObjectByKey<PegawaiView>(_Link);
                return _Pegawai;
            }
        }

        //[PersistentAlias("Link.Nama")]
        //public String Nama
        //{
        //    get
        //    {
        //        return EvaluateAlias("Nama").ToString();
        //    }
        //}

        //[PersistentAlias("Link.Nip")]
        //public int Nip
        //{
        //    get
        //    {
        //        return Convert.ToInt32(EvaluateAlias("Nip"));
        //    }
        //}

        //[PersistentAlias("Link.NamaBagian")]
        //public string NamaBagian
        //{
        //    get
        //    {
        //        return EvaluateAlias("NamaBagian").ToString();
        //    }
        //}

        //[PersistentAlias("Link.NamaJabatan")]
        //public string NamaJabatan
        //{
        //    get
        //    {
        //        return EvaluateAlias("NamaJabatan").ToString();
        //    }
        //}
    }

}