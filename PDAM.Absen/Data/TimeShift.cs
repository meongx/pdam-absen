using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_SchedTimeShift")]
    public class TimeShift : XPObject
    {
        public TimeShift()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public TimeShift(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private bool _IsDayOff;
        private string _Ket;
        private int _DayOrder;
        private double _TimeLength;
        private DateTime _StartTime;
        private ShiftSchedule _Schedule;

        public DateTime StartTime
        {
            get
            {
                return _StartTime;
            }
            set
            {
                SetPropertyValue("StartTime", ref _StartTime, value);
            }
        }

        public double TimeLength
        {
            get
            {
                return _TimeLength;
            }
            set
            {
                SetPropertyValue("TimeLength", ref _TimeLength, value);
            }
        }

        public int DayOrder
        {
            get
            {
                return _DayOrder;
            }
            set
            {
                SetPropertyValue("DayOrder", ref _DayOrder, value);
            }
        }

        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                SetPropertyValue("Ket", ref _Ket, value);
            }
        }

        public bool IsDayOff
        {
            get
            {
                return _IsDayOff;
            }
            set
            {
                SetPropertyValue("IsDayOff", ref _IsDayOff, value);
            }
        }

        [Association("ShiftSchedule-TimeShifts")]
        public ShiftSchedule Schedule
        {
            get
            {
                return _Schedule;
            }
            set
            {
                SetPropertyValue("Schedule", ref _Schedule, value);
            }
        }

        [PersistentAlias("AddHours(StartTime,TimeLength)")]
        public DateTime EndTime
        {
            get
            {
                return Convert.ToDateTime(EvaluateAlias("EndTime"));
            }
        }
    }

}