using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    public enum DayOfWeek
    {
        Senin,
        Selasa,
        Rabu,
        Kamis,
        Jumat,
        Sabtu,
        Minggu
    }

    [Persistent("absen_TimeRegular")]
    public class TimeRegular : XPObject
    {
        public TimeRegular()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public TimeRegular(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private RegularSchedule _Sched;
        private string _Ket;
        private bool _IsDayOff;
        private double _TimeLength;
        private DateTime _StartTime;
        private DayOfWeek _DayOfWeek;

        public DayOfWeek DayOfWeek
        {
            get
            {
                return _DayOfWeek;
            }
            set
            {
                SetPropertyValue("DayOfWeek", ref _DayOfWeek, value);
            }
        }

        public DateTime StartTime
        {
            get
            {
                return _StartTime;
            }
            set
            {
                SetPropertyValue("StartTime", ref _StartTime, value);
            }
        }

        public double TimeLength
        {
            get
            {
                return _TimeLength;
            }
            set
            {
                SetPropertyValue("TimeLength", ref _TimeLength, value);
            }
        }

        public bool IsDayOff
        {
            get
            {
                return _IsDayOff;
            }
            set
            {
                SetPropertyValue("IsDayOff", ref _IsDayOff, value);
            }
        }

        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                SetPropertyValue("Ket", ref _Ket, value);
            }
        }

        [Association("RegularSchedule-TimeRegulars")]
        public RegularSchedule Schedule
        {
            get
            {
                return _Sched;
            }
            set
            {
                SetPropertyValue("Sched", ref _Sched, value);
            }
        }

        [PersistentAlias("AddHours(StartTime,TimeLength)")]
        public DateTime EndTime
        {
            get
            {
                return Convert.ToDateTime(EvaluateAlias("EndTime"));
            }
            set
            {
                var interval = value - _StartTime;
                SetPropertyValue("EndTime", ref _TimeLength, interval.TotalHours);
            }
        }
    }

}