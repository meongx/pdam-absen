using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_RegularSchedule")]
    public class RegularSchedule : XPObject
    {
        public RegularSchedule()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public RegularSchedule(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            var start = new DateTime(9999, 12, 1, 8, 0, 0);
            TimeRegulars.AddRange(
                    new TimeRegular[]
                    {
                        new TimeRegular() { DayOfWeek = DayOfWeek.Senin, StartTime = start, TimeLength = 8 },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Selasa, StartTime = start, TimeLength = 8 },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Rabu, StartTime = start, TimeLength = 8 },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Kamis, StartTime = start, TimeLength = 8 },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Jumat, StartTime = start, TimeLength = 3, Ket = "Setengah Hari" },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Sabtu, StartTime = start, TimeLength = 12, IsDayOff = true, Ket = "Libur" },
                        new TimeRegular() { DayOfWeek = DayOfWeek.Minggu, StartTime = start, TimeLength = 12, IsDayOff = true, Ket = "Libur" }
                    }
                );
        }

        // Fields...
        private string _Name;

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }

        [Association("RegularSchedule-TimeRegulars"),Aggregated]
        public XPCollection<TimeRegular> TimeRegulars
        {
            get
            {
                return GetCollection<TimeRegular>("TimeRegulars");
            }
        }
    }

}