using System;
using System.Text;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    public enum AbsenIzin
    {
        None,
        Terlambat,
        PulangCepat,
        TidakMasuk,
        TerlambatDanPulangCepat
    }

    [Persistent("absen_Absensi")]
    public class Absen : XPObject
    {
        public Absen()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Absen(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private string _KetIzin;
        private AbsenIzin _Izin;
        private int _DayOrder;
        private PegawaiLink _Pegawai;
        private Location _CheckOutLocation;
        private Location _CheckInLocation;
        private DateTime _CheckOutTime;
        private DateTime _CheckInTime;
        private bool _IsDayOff;
        private string _Ket;
        private double _TimeLength;
        private DateTime _StartTime;

        public DateTime StartTime
        {
            get
            {
                return _StartTime;
            }
            set
            {
                SetPropertyValue("StartTime", ref _StartTime, value);
            }
        }

        public double TimeLength
        {
            get
            {
                return _TimeLength;
            }
            set
            {
                SetPropertyValue("TimeLength", ref _TimeLength, value);
            }
        }

        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                SetPropertyValue("Ket", ref _Ket, value);
            }
        }

        public int DayOrder
        {
            get
            {
                return _DayOrder;
            }
            set
            {
                SetPropertyValue("DayOrder", ref _DayOrder, value);
            }
        }

        public bool IsDayOff
        {
            get
            {
                return _IsDayOff;
            }
            set
            {
                SetPropertyValue("IsDayOff", ref _IsDayOff, value);
            }
        }

        public DateTime CheckInTime
        {
            get
            {
                return _CheckInTime;
            }
            set
            {
                SetPropertyValue("CheckInTime", ref _CheckInTime, value);
            }
        }

        public DateTime CheckOutTime
        {
            get
            {
                return _CheckOutTime;
            }
            set
            {
                SetPropertyValue("CheckOutTime", ref _CheckOutTime, value);
            }
        }

        public Location CheckInLocation
        {
            get
            {
                return _CheckInLocation;
            }
            set
            {
                SetPropertyValue("CheckInLocation", ref _CheckInLocation, value);
            }
        }

        public Location CheckOutLocation
        {
            get
            {
                return _CheckOutLocation;
            }
            set
            {
                SetPropertyValue("CheckOutLocation", ref _CheckOutLocation, value);
            }
        }

        public AbsenIzin Izin
        {
            get
            {
                return _Izin;
            }
            set
            {
                SetPropertyValue("Izin", ref _Izin, value);
            }
        }

        public string KetIzin
        {
            get
            {
                return _KetIzin;
            }
            set
            {
                SetPropertyValue("KetIzin", ref _KetIzin, value);
            }
        }

        [Association("PegawaiLink-Absens")]
        public PegawaiLink Pegawai
        {
            get
            {
                return _Pegawai;
            }
            set
            {
                SetPropertyValue("PegawaiLink", ref _Pegawai, value);
            }
        }

        [PersistentAlias("AddHours(StartTime,TimeLength)")]
        public DateTime EndTime
        {
            get
            {
                return Convert.ToDateTime(EvaluateAlias("EndTime"));
            }
        }

        [NonPersistent]
        public DateTime iStartTime
        {
            get
            {
                return new DateTime(9999,11,1,_StartTime.Hour,_StartTime.Minute,_StartTime.Second);
            }
        }

        [NonPersistent]
        public DateTime iEndTime
        {
            get
            {
                return iStartTime.AddHours(_TimeLength);
            }
        }

        [NonPersistent]
        public TimeSpan TotalWorkHour
        {
            get
            {
                if (_CheckInTime.Year == 1 || _CheckOutTime.Year == 1)
                    return TimeSpan.Zero;

                return _CheckOutTime - _CheckInTime;
            }
        }

        [NonPersistent]
        public TimeSpan StrictWorkHour
        {
            get
            {
                if (_CheckInTime.Year == 1 || _CheckOutTime.Year == 1)
                    return TimeSpan.Zero;

                var t1 = _CheckInTime <= _StartTime ? _StartTime : _CheckInTime;
                var t2 = _CheckOutTime >= EndTime ? EndTime : _CheckOutTime;

                return t2 - t1;
            }
        }

        [NonPersistent]
        public TimeSpan TotalScheduledHour
        {
            get
            {
                return EndTime - StartTime;
            }
        }

        [NonPersistent]
        public TimeSpan Terlambat
        {
            get
            {
                if (_CheckInTime <= _StartTime || _CheckInTime.Year == 1 || _Izin == AbsenIzin.Terlambat || _Izin == AbsenIzin.TerlambatDanPulangCepat || _Izin == AbsenIzin.TidakMasuk)
                    return TimeSpan.Zero;

                return _CheckInTime - _StartTime;
            }
        }

        [NonPersistent]
        public TimeSpan PulangCepat
        {
            get
            {
                if (_CheckOutTime >= EndTime || _CheckOutTime.Year == 1 || _Izin == AbsenIzin.PulangCepat || _Izin == AbsenIzin.TerlambatDanPulangCepat || _Izin == AbsenIzin.TidakMasuk)
                    return TimeSpan.Zero;

                return EndTime - _CheckOutTime;
            }
        }

        [NonPersistent]
        public double iTerlambat
        {
            get
            {
                return Terlambat.TotalSeconds;
            }
        }

        [NonPersistent]
        public double iPulangCepat
        {
            get
            {
                return PulangCepat.TotalSeconds;
            }
        }

        [NonPersistent]
        public bool IsViolation
        {
            get
            {
                if (IsDayOff || _Izin == AbsenIzin.TidakMasuk)
                    return false;

                else if 
                    (
                        (
                            _CheckInTime.Year == 1 
                            || _CheckOutTime.Year == 1 
                            || (_CheckOutTime < EndTime && _Izin != AbsenIzin.PulangCepat && _Izin != AbsenIzin.TerlambatDanPulangCepat)
                            || (_CheckInTime > StartTime && _Izin != AbsenIzin.Terlambat && _Izin != AbsenIzin.TerlambatDanPulangCepat)
                        ) 
                        && _StartTime.Date < DateTime.Now.Date
                    )
                    return true;

                return false;
            }
        }

        [NonPersistent]
        public string ReportSummary
        {
            get
            {
                if (_IsDayOff) 
                    return "Libur Kerja";

                if (_Izin == AbsenIzin.TidakMasuk)
                {
                    if (_KetIzin.Length == 0)
                        return "Izin Tidak Masuk";
                    else
                        return "Izin Tidak Masuk (" + _KetIzin + ")";
                }

                if (_CheckInTime.Year == 1 && _CheckOutTime.Year == 1 && DateTime.Now > _StartTime)
                    return "Mangkir";

                var summary = new StringBuilder();

                if (_CheckInTime.Year == 1 && DateTime.Now > _StartTime)
                    summary.Append("No CheckIn");
                if (_CheckOutTime.Year == 1 && DateTime.Now > _StartTime)
                    summary.Append("No CheckOut");

                if (_CheckInTime > _StartTime)
                {
                    var span = _CheckInTime - _StartTime;

                    if (summary.Length > 0) summary.Append("; ");

                    if (_Izin == AbsenIzin.Terlambat || _Izin == AbsenIzin.TerlambatDanPulangCepat)
                        summary.Append("Izin ");

                    summary.Append("Terlambat ");
                    if (span.Hours > 0)
                        summary.Append(span.Hours + " jam ");
                    if (span.Minutes > 0)
                        summary.Append(span.Minutes + " menit");

                    if (_Izin == AbsenIzin.Terlambat && _KetIzin.Length > 0)
                        summary.Append(" ("+_KetIzin+")");
                }
                if (_CheckOutTime < EndTime && _CheckOutTime.Year != 1)
                {
                    var span = EndTime - _CheckOutTime;

                    if (summary.Length > 0) summary.Append("; ");

                    if (_Izin == AbsenIzin.PulangCepat || _Izin == AbsenIzin.TerlambatDanPulangCepat)
                        summary.Append("Izin ");
                    
                    summary.Append("Pulang Cepat ");
                    if (span.Hours > 0)
                        summary.Append(span.Hours + " jam ");
                    if (span.Minutes > 0)
                        summary.Append(span.Minutes + " menit");

                    if (_Izin == AbsenIzin.PulangCepat && _KetIzin.Length > 0)
                        summary.Append(" (" + _KetIzin + ")");
                }

                if (_Izin == AbsenIzin.TerlambatDanPulangCepat && _KetIzin.Length > 0)
                    summary.Append(" (" + _KetIzin + ")");

                return summary.ToString();
            }
        }
    }

}