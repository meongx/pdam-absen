using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    //[DeferredDeletion(Enabled=false)]
    [Persistent("absen_Location")]
    public class Location : XPObject
    {
        public Location()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Location(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private string _Name;
        private int _Code;

        //[Key(false)]
        //[Indexed(Unique=true)]
        public int Code
        {
            get
            {
                return _Code;
            }
            set
            {
                SetPropertyValue("Code", ref _Code, value);
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
    }

}