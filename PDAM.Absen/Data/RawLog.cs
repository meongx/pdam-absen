using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("absen_RawLog")]
    public class RawLog : XPLiteObject
    {
        public RawLog()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public RawLog(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        // Fields...
        private int _Oid;
        private int _Location;
        private string _Nip;
        private DateTime _Time;

        [Key(true)]
        public int Oid
        {
            get
            {
                return _Oid;
            }
            set
            {
                SetPropertyValue("Oid", ref _Oid, value);
            }
        }

        public DateTime Time
        {
            get
            {
                return _Time;
            }
            set
            {
                SetPropertyValue("Time", ref _Time, value);
            }
        }

        public string Nip
        {
            get
            {
                return _Nip;
            }
            set
            {
                SetPropertyValue("Nip", ref _Nip, value);
            }
        }

        public int Location
        {
            get
            {
                return _Location;
            }
            set
            {
                SetPropertyValue("Location", ref _Location, value);
            }
        }
    }

}