using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{
    [Persistent("bag")]
    public class Bagian : XPLiteObject
    {
        public Bagian()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Bagian(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private string _Nama;
        private string _Pos;
        private string _Kode;
        private int _ID;

        [Key]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                SetPropertyValue("ID", ref _ID, value);
            }
        }

        [Persistent("KODE_BAG")]
        public string Kode
        {
            get
            {
                return _Kode;
            }
            set
            {
                SetPropertyValue("Kode", ref _Kode, value);
            }
        }

        [Persistent("KODE_POS")]
        public string Pos
        {
            get
            {
                return _Pos;
            }
            set
            {
                SetPropertyValue("Pos", ref _Pos, value);
            }
        }

        [Persistent("NAMA_BAG")]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                SetPropertyValue("Nama", ref _Nama, value);
            }
        }
    }

}