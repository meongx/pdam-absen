using System;
using DevExpress.Xpo;

namespace PDAM.Absen.Data
{

    public class Libur : XPObject
    {
        public Libur()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Libur(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        // Fields...
        private string _Ket;
        private DateTime _Tanggal;

        public DateTime Tanggal
        {
            get
            {
                return _Tanggal;
            }
            set
            {
                SetPropertyValue("Tanggal", ref _Tanggal, value);
            }
        }

        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                SetPropertyValue("Ket", ref _Ket, value);
            }
        }
    }

}