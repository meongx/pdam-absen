﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
//using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ProcessLog.xaml
    /// </summary>
    public partial class ProcessLog : Window
    {
        private string filePath;

        private BackgroundWorker worker = new BackgroundWorker();

        enum DetermineTimeResult
        {
            CloserToFirst,
            CloserToSecond
        }

        public ProcessLog()
        {
            InitializeComponent();

            var s = Properties.Settings.Default.InputLogFileName;
            if (!String.IsNullOrEmpty(s))
            {
                filePath = s;
                tFilePath.Text = Path.GetFileName(filePath);
                tStatus.Text = "File Log:";
            }

            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            tStatus.Text = "Process Completed";
            button1.IsEnabled = true;
            //button1.Content = "Proses";
            Cursor = Cursors.Arrow;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            ProcessLogFile(filePath);
            ApplyAbsen();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(filePath))
            {
                MessageBox.Show("Silahkan pilih file dengan menggeser (Drag and Drop) file ke window (jendela) program");
                return;
            }

            Cursor = Cursors.Wait;
            //button1.Content = "Memproses...";
            tStatus.Text = "Memproses...";
            button1.IsEnabled = false;

            System.Threading.Thread.Sleep(100);
            worker.RunWorkerAsync();

            //ProcessLogFile(filePath);
            //ApplyAbsen();

            //button1.IsEnabled = true;
            //button1.Content = "Proses";
            //Cursor = Cursors.Arrow;
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                filePath = (e.Data.GetData(DataFormats.FileDrop, true) as string[])[0];
                tFilePath.Text = Path.GetFileName(filePath);
                tStatus.Text = "File Log:";
            }
        }

        private void ProcessLogFile(string filename)
        {
            if (!File.Exists(filename))
            {
                MessageBox.Show("File Tidak Ditemukan");
                return;
            }

            using (var s = new StreamReader(filename))
            {
                int count = 0;
                string line;
                var collection = new XPCollection<Data.RawLog>();

                while ((line = s.ReadLine()) != null)
                {
                    if (line.Length == 0) continue;

                    var dt = new DateTime(
                            int.Parse(line.Substring(0, 4)),
                            int.Parse(line.Substring(4, 2)),
                            int.Parse(line.Substring(6, 2)),
                            int.Parse(line.Substring(8, 2)),
                            int.Parse(line.Substring(10, 2)),
                            0
                        );
                    var nip = int.Parse(line.Substring(12, 4));
                    var loc = int.Parse(line.Substring(16, 4));

                    collection.Filter = CriteriaOperator.Parse(
                        "[Time] == ? And [Nip] == ? And [Location] == ?",
                        dt,nip,loc
                    );
                    collection.Reload();

                    // exact entry exists, skip
                    if (collection.Count != 0) continue;

                    var log = new Data.RawLog()
                    {
                        Time = dt,
                        Nip = String.Format("{0:000}", nip),
                        Location = loc
                    };
                    log.Save();

                    if (++count % 100 > 1)
                        worker.ReportProgress(0, "Reading File: " + count + " lines processed");
                }
            }
            
            Properties.Settings.Default.InputLogFileName = filename;
            Properties.Settings.Default.Save();
        }

        private void ApplyAbsen()
        {
            int count = 0;
            var ticks = DateTime.Now;

            var logs = new XPCollection<Data.RawLog>();
            if (logs.Count == 0) return;

            var deleteLog = new List<Data.RawLog>();

            foreach (var log in logs)
            {
                var absen = new XPCollection<Data.Absen>();
                absen.Filter = CriteriaOperator.Parse(
                    "[Pegawai.Link] = ? AND (GetDate(StartTime) = ? OR GetDate(AddHours(StartTime,TimeLength)) = ?)",
                    log.Nip, log.Time.Date, log.Time.Date
                );
                absen.Reload();

                var location = XpoDefault.Session.GetObjectByKey<Data.Location>(log.Location);
                if (location == null)
                {
                    location = new Data.Location()
                    {
                        Code = log.Location,
                        Name = "Unknown"
                    };
                    location.Save();
                }

                if (absen.Count == 1)
                {
                    if (WhichIsCloser(absen[0].StartTime, absen[0].EndTime, log.Time) == DetermineTimeResult.CloserToFirst) // mostlikely checkin
                    {
                        ApplyCheckIn(absen[0], log.Time, location);
                    }
                    else // mostlikely checkout
                    {
                        ApplyCheckOut(absen[0], log.Time, location);
                    }
                }
                else if (absen.Count == 2)
                {
                    if (WhichIsCloser(absen[0].EndTime, absen[1].StartTime, log.Time) == DetermineTimeResult.CloserToFirst || absen[1].IsDayOff) // mostlikely checkout of first entry
                    {
                        ApplyCheckOut(absen[0], log.Time, location);
                    }
                    else // mostlikely checkin of second entry
                    {
                        ApplyCheckIn(absen[1], log.Time, location);
                    }
                }

                if (absen.Count != 0)
                    deleteLog.Add(log);

                //if (++count % 100 > 1)
                //    worker.ReportProgress(0, "Applying Data: " + count + " entries processed");
                count++;
                if ((DateTime.Now - ticks).TotalSeconds > 2)
                {
                    worker.ReportProgress(0, "Applying Data: " + count + " entries processed");
                    ticks = DateTime.Now;
                }
            }

            foreach (var log in deleteLog)
                log.Delete();
        }

        private void ApplyCheckIn(Data.Absen absen, DateTime time, Data.Location location)
        {
            if (absen.CheckInTime.Year == 1) // checkin not set
            {
                absen.CheckInTime = time;
                absen.CheckInLocation = location;
                absen.Save();
            }
            else // checkin is set
            {
                var count = Convert.ToInt16(XpoDefault.Session.Evaluate(
                    typeof(Data.RawLog),
                    CriteriaOperator.Parse("COUNT()"),
                    CriteriaOperator.Parse("[Nip] == ? And GetDate(Time) == ? And [Time] > ?", absen.Pegawai.Link, time.Date, time)
                ));
                if (count == 0) // no later time, means early checkout
                {
                    absen.CheckOutTime = time;
                    absen.CheckOutLocation = location;
                    absen.Save();
                }
            }
        }

        private void ApplyCheckOut(Data.Absen absen, DateTime time, Data.Location location)
        {
            if (absen.CheckInTime.Year == 1) // checkin not set
            {
                var count = Convert.ToInt16(XpoDefault.Session.Evaluate(
                    typeof(Data.RawLog),
                    CriteriaOperator.Parse("COUNT()"),
                    CriteriaOperator.Parse("[Nip] == ? And GetDate(Time) == ? And [Time] > ? And GetHour([Time] - ?) > 0", absen.Pegawai.Link, time.Date, time, time)
                ));
                if (count > 0)
                {
                    absen.CheckInTime = time;
                    absen.CheckInLocation = location;
                    absen.Save();
                }
            }
            else if (absen.CheckOutTime.Year == 1) // checkout not set
            {
                absen.CheckOutTime = time;
                absen.CheckOutLocation = location;
                absen.Save();
            }
        }

        private DetermineTimeResult WhichIsCloser(DateTime firstTime, DateTime secondTime, DateTime time)
        {
            var t1 = (time - firstTime).Duration();
            var t2 = (time - secondTime).Duration();

            if (t1 < t2)
            {
                return DetermineTimeResult.CloserToFirst;
            }

            return DetermineTimeResult.CloserToSecond;
        }
    }
}
