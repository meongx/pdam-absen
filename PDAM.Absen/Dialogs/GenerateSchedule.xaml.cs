﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for GenerateSchedule.xaml
    /// </summary>
    public partial class GenerateSchedule : Window
    {
        private BackgroundWorker worker = new BackgroundWorker();

        private delegate void UpdateProgressBarDelegate(DependencyProperty dp, Object value);
        //private UpdateProgressBarDelegate progressDelegate;

        public GenerateSchedule()
        {
            InitializeComponent();

            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            //progressDelegate = new UpdateProgressBarDelegate(progressBar1.SetValue);
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Cursor = Cursors.Arrow;
            button1.IsEnabled = true;
            progressBar1.Visibility = System.Windows.Visibility.Hidden;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                if (progressBar1.Maximum == 0)
                {
                    progressBar1.Visibility = System.Windows.Visibility.Visible;
                    //progressBar1.Maximum = (e.UserState as int?).Value;
                    progressBar1.Maximum = Convert.ToDouble(e.UserState);
                }
                else
                {
                    progressBar1.Value = Convert.ToDouble(e.UserState);
                    //Dispatcher.Invoke(
                    //    progressDelegate,
                    //    System.Windows.Threading.DispatcherPriority.Background,
                    //    new object[] { ProgressBar.ValueProperty, Convert.ToDouble(e.UserState) }
                    //);
                }
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = (sender as BackgroundWorker);
            var args = (e.Argument as GenerateArgs?).Value;

            var processed = 0;
            var tick = DateTime.Now;

            var transaction = new UnitOfWork();
            var freeDays = new XPCollection<Data.FreeDay>(transaction);
            var pegs = new XPCollection(transaction,typeof(Data.PegawaiLink));
            pegs.Load();

            // total days to generate
            worker.ReportProgress(0, pegs.Count * (args.EndDate - args.StartDate).Duration().TotalDays);

            foreach (Data.PegawaiLink peg in pegs)
            {
                if (peg.ShiftSchedule != null)
                {
                    XPCollection<Data.TimeShift> shifts = peg.ShiftSchedule.TimeShifts;
                    shifts.Sorting.Add(new SortProperty("DayOrder", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    shifts.Load();

                    for (DateTime tgl = args.StartDate.Date; tgl <= args.EndDate.Date; tgl = tgl.AddDays(1))
                    {
                        var existing = Convert.ToInt32(
                            transaction.Evaluate(
                                typeof(Data.Absen), 
                                CriteriaOperator.Parse("COUNT()"),
                                CriteriaOperator.Parse("GetDate(StartTime) == ? && [Pegawai] == ?", tgl.Date, peg)
                            )
                        );

                        if (existing == 0)
                        {
                            //var nextShift = shifts[peg.NextDayOrder];
                            var nextShift = shifts.First(s => s.DayOrder == peg.NextDayOrder + 1);

                            freeDays.Filter = CriteriaOperator.Parse(
                                "[AffectsShift] == true And ([Month] == ? And [Day] == ? And ([Year] == 0 Or [Year] == ?))",
                                tgl.Month, tgl.Day, tgl.Year
                            );

                            var absen = new Data.Absen(transaction)
                                {
                                    StartTime = tgl.Add(new TimeSpan(nextShift.StartTime.Hour, nextShift.StartTime.Minute, 0)),
                                    TimeLength = nextShift.TimeLength,
                                    Ket = (freeDays.Count == 0 ? nextShift.Ket : freeDays[0].Ket),
                                    DayOrder = peg.NextDayOrder,
                                    IsDayOff = (freeDays.Count == 0 ? nextShift.IsDayOff : true)
                                };
                            peg.Absens.Add(absen);

                            if (++peg.NextDayOrder >= shifts.Count) peg.NextDayOrder = 0;
                        }
                    }
                }
                else if (peg.RegularSchedule != null)
                {
                    XPCollection<Data.TimeRegular> shifts = peg.RegularSchedule.TimeRegulars;

                    for (DateTime tgl = args.StartDate.Date; tgl <= args.EndDate.Date; tgl = tgl.AddDays(1))
                    {
                        var existing = Convert.ToInt32(
                            transaction.Evaluate(
                                typeof(Data.Absen),
                                CriteriaOperator.Parse("COUNT()"),
                                CriteriaOperator.Parse("GetDate(StartTime) == ? && [Pegawai] == ?", tgl.Date, peg)
                            )
                        );

                        if (existing == 0)
                        {
                            freeDays.Filter = CriteriaOperator.Parse(
                                "[AffectsRegular] == true And ([Month] == ? And [Day] == ? And ([Year] == 0 Or [Year] == ?))",
                                tgl.Month, tgl.Day, tgl.Year
                            );

                            var day = shifts.First(s =>
                            {
                                if (s.DayOfWeek == Data.DayOfWeek.Senin && tgl.DayOfWeek == DayOfWeek.Monday) return true;
                                if (s.DayOfWeek == Data.DayOfWeek.Minggu && tgl.DayOfWeek == DayOfWeek.Sunday) return true;
                                if (Convert.ToByte(s.DayOfWeek) == Convert.ToByte(tgl.DayOfWeek) - 1) return true;

                                return false;
                            });

                            var absen = new Data.Absen(transaction)
                            {
                                StartTime = tgl.Add(new TimeSpan(day.StartTime.Hour, day.StartTime.Minute, 0)),
                                TimeLength = day.TimeLength,
                                Ket = (freeDays.Count == 0 ? day.Ket : freeDays[0].Ket),
                                IsDayOff = (freeDays.Count == 0 ? day.IsDayOff : true)
                            };
                            peg.Absens.Add(absen);
                        }
                    }
                }
                transaction.CommitChanges();

                processed++;
                if ((DateTime.Now - tick).TotalSeconds >= 2)
                {
                    worker.ReportProgress(0, processed);
                    tick = DateTime.Now;
                }
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (dateEdit1.EditValue == null || dateEdit2.EditValue == null) return;

            Cursor = Cursors.Wait;
            button1.IsEnabled = false;
            progressBar1.Visibility = System.Windows.Visibility.Visible;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 0;

            worker.RunWorkerAsync(new GenerateArgs
            {
                StartDate = (dateEdit1.EditValue as DateTime?).Value,
                EndDate = (dateEdit2.EditValue as DateTime?).Value,
                //OverwriteExisting = radioButton2.IsChecked.Value
            });
        }

        struct GenerateArgs
        {
            public DateTime StartDate;
            public DateTime EndDate;
            //public bool OverwriteExisting;
        }
    }
}
