﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ManualPermission.xaml
    /// </summary>
    public partial class ManualPermission : Window
    {
        private Data.Absen absen;

        public ManualPermission()
        {
            InitializeComponent();

            lookUpEdit1.ItemsSource = Data.Pegawai.GetDataView();

            var list = new Dictionary<String,Data.AbsenIzin>()
            {
                { "Batalkan Izin", Data.AbsenIzin.None },
                { "Terlambat", Data.AbsenIzin.Terlambat },
                { "Pulang Cepat", Data.AbsenIzin.PulangCepat },
                { "Tidak Masuk", Data.AbsenIzin.TidakMasuk },
                { "Terlambat & Pulang Cepat", Data.AbsenIzin.TerlambatDanPulangCepat }
            };
            comboBoxEdit1.ItemsSource = list;
        }

        private void lookUpEdit1_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            OnSourceChanged();
        }

        private void dateEdit1_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            OnSourceChanged();
        }

        private void OnSourceChanged()
        {
            if (lookUpEdit1.EditValue == null || dateEdit1.EditValue == null)
                return;

            var pegawai = lookUpEdit1.EditValue as Data.Pegawai;
            var dt = dateEdit1.EditValue as DateTime?;

            var collection = new XPCollection<Data.Absen>();
            collection.Filter = CriteriaOperator.Parse(
                "[Pegawai.Link] = ? And GetDate(StartTime) = ?",
                pegawai.Nip, dt.Value.Date
            );
            collection.Load();

            if (collection.Count == 0)
            {
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan belum di-Generate",
                    "",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return;
            }

            absen = collection[0];

            if (absen.IsDayOff)
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan adalah Hari Libur",
                    "",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning
                );
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var izin = comboBoxEdit1.EditValue as Data.AbsenIzin?;

            if (absen == null || izin == null)
            {
                MessageBox.Show("Silahkan pilih Pegawai dan Tanggal");
                return;
            }

            Cursor = Cursors.Wait;
            absen.Izin = izin.Value;
            absen.KetIzin = textEdit1.Text;
            absen.Save();
            Cursor = Cursors.Arrow;
        }
    }
}
