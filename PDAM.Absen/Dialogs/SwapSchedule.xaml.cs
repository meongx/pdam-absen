﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for SwapSchedule.xaml
    /// </summary>
    public partial class SwapSchedule : Window
    {
        private Data.Absen absen1, absen2;
        public SwapSchedule()
        {
            InitializeComponent();

            var collection = Data.Pegawai.GetDataView();
            lookUpEdit1.ItemsSource = collection;
            lookUpEdit2.ItemsSource = collection;
        }

        private void DetermineData1(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (lookUpEdit1.EditValue == null || dateEdit1.EditValue == null)
                return;

            var pegawai = lookUpEdit1.EditValue as Data.Pegawai;
            var tgl = (dateEdit1.EditValue as DateTime?).Value;

            var collection = new XPCollection<Data.Absen>();
            collection.Filter = CriteriaOperator.Parse(
                "[Pegawai.Link] = ? And GetDate(StartTime) = ?",
                pegawai.Nip, tgl.Date
            );
            collection.Load();

            if (collection.Count == 0)
            {
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan belum di-Generate",
                    "",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return;
            }
            absen1 = collection[0];
            textBlock1.Text = string.Format("{0:HH:mm} - {1:HH:mm} {2}", absen1.StartTime, absen1.EndTime, absen1.Ket);
        }

        private void DetermineData2(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (lookUpEdit2.EditValue == null || dateEdit2.EditValue == null)
                return;

            var pegawai = lookUpEdit2.EditValue as Data.Pegawai;
            var tgl = (dateEdit2.EditValue as DateTime?).Value;

            var collection = new XPCollection<Data.Absen>();
            collection.Filter = CriteriaOperator.Parse(
                "[Pegawai.Link] = ? And GetDate(StartTime) = ?",
                pegawai.Nip, tgl.Date
            );
            collection.Load();

            if (collection.Count == 0)
            {
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan belum di-Generate",
                    "",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return;
            }
            absen2 = collection[0];
            textBlock2.Text = string.Format("{0:HH:mm} - {1:HH:mm} {2}", absen2.StartTime, absen2.EndTime, absen2.Ket);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (absen1 == null || absen2 == null)
            {
                MessageBox.Show("Silahkan pilih kedua Pegawai dan Tanggal");
                return;
            }

            /*if (absen1.CheckInTime.Year != 1 || absen2.CheckInTime.Year != 1)
            {
                MessageBox.Show("Data telah berisi Checkin, tidak dapat melakukan penukaran");
                return;
            }*/

            if (absen1.Pegawai.Link == absen2.Pegawai.Link && absen1.StartTime == absen2.StartTime)
            {
                MessageBox.Show("Kedua data sama, tidak dapat melakukan penukaran");
                return;
            }

            Cursor = Cursors.Wait;
            var temp = new
            {
                Ket = absen1.Ket,
                StartTime = absen1.StartTime,
                TimeLength = absen1.TimeLength,
                IsDayOff = absen1.IsDayOff
            };
            
            var dt = absen1.StartTime.Date;

            absen1.Ket = absen2.Ket;
            absen1.StartTime = dt.Add(new TimeSpan(absen2.StartTime.Hour, absen2.StartTime.Minute, absen2.StartTime.Second));
            absen1.TimeLength = absen2.TimeLength;
            absen1.IsDayOff = absen2.IsDayOff;

            dt = absen2.StartTime.Date;

            absen2.Ket = temp.Ket;
            absen2.StartTime = dt.Add(new TimeSpan(temp.StartTime.Hour, temp.StartTime.Minute, temp.StartTime.Second));
            absen2.TimeLength = temp.TimeLength;
            absen2.IsDayOff = temp.IsDayOff;

            absen1.Save();
            absen2.Save();

            textBlock1.Text = string.Format("{0:HH:mm} - {1:HH:mm} {2}", absen1.StartTime, absen1.EndTime, absen1.Ket);
            textBlock2.Text = string.Format("{0:HH:mm} - {1:HH:mm} {2}", absen2.StartTime, absen2.EndTime, absen2.Ket);

            Cursor = Cursors.Arrow;
        }
    }
}
