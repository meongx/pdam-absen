﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for RegularSchedEditor.xaml
    /// </summary>
    public partial class RegularSchedEditor : Window
    {
        private XPCollection collection;

        public RegularSchedEditor(XPCollection collection)
        {
            InitializeComponent();

            this.collection = collection;
            DataContext = new Data.RegularSchedule();
        }

        public RegularSchedEditor(Data.RegularSchedule data)
        {
            InitializeComponent();

            DataContext = data;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;

            var data = (DataContext as Data.RegularSchedule);
            data.Save();

            if (collection != null) collection.Add(data);

            Cursor = Cursors.Arrow;
        }
    }
}
