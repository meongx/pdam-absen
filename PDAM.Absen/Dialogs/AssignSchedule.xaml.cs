﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;
using DevExpress.Data;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for AssignSchedule.xaml
    /// </summary>
    public partial class AssignSchedule : Window
    {
        private XPCollection dataPegawai;
        private XPCollection dataShiftSchedule;
        private XPCollection dataRegulerSchedule;
        private BindingList<Data.PegawaiView> draggedRows = new BindingList<Data.PegawaiView>();

        private Point startPoint;
        private bool isDragging;

        private const string filterString = "[<PegawaiLink>][Link == ^.Nip].Count == 0";

        public AssignSchedule()
        {
            InitializeComponent();

            listBox1.ItemsSource = draggedRows;

            PrepareShiftSchedule();

            dataPegawai = new XPCollection(typeof(Data.PegawaiView))
            {
                DeleteObjectOnRemove = false,
                Filter = CriteriaOperator.Parse(filterString)
            };
            gridControl1.ItemsSource = dataPegawai;
        }

        private void PrepareShiftSchedule()
        {
            if (dataShiftSchedule == null) dataShiftSchedule = new XPCollection(typeof(Data.ShiftSchedule));
            comboBoxEdit1.ItemsSource = dataShiftSchedule;

            layoutItem2.Visibility = System.Windows.Visibility.Visible;
        }

        private void checkBox1_Unchecked(object sender, RoutedEventArgs e)
        {
            dataPegawai.Filter = null;
            dataPegawai.Reload();
        }

        private void tableView1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed &&
                tableView1.GetRowHandleByMouseEventArgs(e) != GridDataController.InvalidRow)
            {
                startPoint = e.GetPosition(null);
                isDragging = true;
            }
        }

        private void tableView1_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (isDragging && gridControl1.SelectedItems.Count > 0 &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                try
                {
                    //var selected = tableView1.SelectedRows.ToList<object>();
                    var selected = gridControl1.SelectedItems.Cast<object>().ToList<object>();
                    var dragData = new DataObject("meongDropFormat", selected);
                    DragDrop.DoDragDrop(tableView1.GetRowElementByMouseEventArgs(e), dragData, DragDropEffects.Move);
                }
                catch
                {
                }
                finally
                {
                    isDragging = false;
                }
            }
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("meongDropFormat") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void listBox1_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("meongDropFormat"))
            {
                var data = e.Data.GetData("meongDropFormat") as List<object>;
                //var data = e.Data.GetData("meongDropFormat") as List;
                foreach(Data.PegawaiView row in data)
                {
                    if (!draggedRows.Contains(row))
                        draggedRows.Add(row);
                    if (checkBox1.IsChecked.Value)
                        dataPegawai.Remove(row);
                }
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            foreach (Data.PegawaiView row in draggedRows)
            {
                dataPegawai.Add(row);
            }
            draggedRows.Clear();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (//draggedRows.Count == 0 ||
                comboBoxEdit1.SelectedItem == null ||
                (radioButton1.IsChecked.Value && comboBoxEdit2.SelectedItem == null)
            )
            {
                MessageBox.Show("Silahkan pilih jenis Jadwal dan Next Shift (untuk jadwal Shift)","",MessageBoxButton.OK,MessageBoxImage.Warning);
                return;
            }

            Cursor = Cursors.Wait;

            var xpcol = new XPCollection<Data.PegawaiLink>();
            Data.PegawaiLink link;

            foreach (Data.PegawaiView row in draggedRows)
            {
                xpcol.Filter = CriteriaOperator.Parse("Link == ?",row.Nip);
                xpcol.Reload();

                if (xpcol.Count > 0)
                {
                    link = xpcol[0];
                }
                else
                {
                    link = new Data.PegawaiLink()
                        {
                            Link = row.Nip
                        };
                }

                if (radioButton1.IsChecked.Value)
                {
                    link.RegularSchedule = null;
                    link.ShiftSchedule = comboBoxEdit1.SelectedItem as Data.ShiftSchedule;
                    link.NextDayOrder = (comboBoxEdit2.SelectedItem as Data.TimeShift).DayOrder - 1;
                }
                else
                {
                    link.ShiftSchedule = null;
                    link.RegularSchedule = comboBoxEdit1.SelectedItem as Data.RegularSchedule;
                }

                link.Save();
            }
            draggedRows.Clear();

            Cursor = Cursors.Arrow;
        }

        private void checkBox1_Checked(object sender, RoutedEventArgs e)
        {
            if (dataPegawai != null)
            {
                dataPegawai.Filter = CriteriaOperator.Parse(filterString);
                dataPegawai.Reload();
            }
        }

        private void comboBoxEdit1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selected = comboBoxEdit1.SelectedItem as Data.ShiftSchedule;
            if (selected == null || selected.TimeShifts.Sorting.Count > 0) return;

            selected.TimeShifts.Sorting.Add(new SortProperty("DayOrder",DevExpress.Xpo.DB.SortingDirection.Ascending));
        }

        private void listBox1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                startPoint = e.GetPosition(null);
                isDragging = true;
            }
        }

        private void listBox1_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (isDragging && listBox1.SelectedItem != null &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                try
                {
                    var selected = listBox1.SelectedItem;
                    var dragData = new DataObject("meongDropFormatBack", selected);
                    DragDrop.DoDragDrop(listBox1, dragData, DragDropEffects.Move);
                }
                catch
                {
                }
                finally
                {
                    isDragging = false;
                }
            }
        }

        private void tableView1_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("meongDropFormatBack") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void tableView1_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("meongDropFormatBack"))
            {
                var data = e.Data.GetData("meongDropFormatBack") as Data.PegawaiView;
                dataPegawai.Add(data);
                draggedRows.Remove(data);
            }
        }

        private void radioButton2_Checked(object sender, RoutedEventArgs e)
        {
            if (dataRegulerSchedule == null) dataRegulerSchedule = new XPCollection(typeof(Data.RegularSchedule));
            comboBoxEdit1.ItemsSource = dataRegulerSchedule;

            layoutItem2.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                PrepareShiftSchedule();
        }
    }
}
