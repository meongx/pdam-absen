﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ManualPresence.xaml
    /// </summary>
    public partial class ManualPresence : Window
    {
        private Data.Absen absen;

        public ManualPresence()
        {
            InitializeComponent();

            lookUpEdit1.ItemsSource = Data.Pegawai.GetDataView();

            var lokasi = new XPCollection(typeof(Data.Location));
            comboBoxEdit1.ItemsSource = lokasi;
            comboBoxEdit2.ItemsSource = lokasi;
        }

        private void lookUpEdit1_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            OnSourceChanged();
        }

        private void dateEdit1_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            OnSourceChanged();
        }

        private void OnSourceChanged()
        {
            if (lookUpEdit1.EditValue == null || dateEdit1.EditValue == null)
                return;

            var pegawai = lookUpEdit1.EditValue as Data.Pegawai;
            var dt = dateEdit1.EditValue as DateTime?;
            
            var collection = new XPCollection<Data.Absen>();
            collection.Filter = CriteriaOperator.Parse(
                "[Pegawai.Link] = ? And GetDate(StartTime) = ?",
                pegawai.Nip, dt.Value.Date
            );
            collection.Load();

            if (collection.Count == 0)
            {
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan belum di-Generate", 
                    "", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Error
                );
                return;
            }

            absen = collection[0];
            if (absen.CheckInTime.Year == 1)
                textEdit1.EditValue = absen.StartTime;
            else
                textEdit1.EditValue = absen.CheckInTime;

            if (absen.CheckOutTime.Year == 1)
                textEdit2.EditValue = absen.EndTime;
            else
                textEdit2.EditValue = absen.CheckOutTime;

            if (absen.IsDayOff)
                MessageBox.Show(
                    "Jadwal untuk Pegawai dan Tanggal yang ditentukan adalah Hari Libur",
                    "",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning
                );
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var c1 = checkEdit1.IsChecked;
            var c2 = checkEdit2.IsChecked;

            if ((c1 == null || c1.Value == false) &&
                (c2 == null || c2.Value == false))
            {
                return;
            }

            Cursor = Cursors.Wait;

            var checkin = textEdit1.EditValue as DateTime?;
            var checkout = textEdit2.EditValue as DateTime?;

            if (c1.Value && checkin != null)
            {
                absen.CheckInTime = checkin.Value;
                absen.CheckInLocation = comboBoxEdit1.EditValue as Data.Location;
            }

            if (c2.Value && checkout != null)
            {
                if (absen.StartTime.Date != absen.EndTime.Date)
                {
                    var midnight = absen.EndTime.Date - new TimeSpan(0,1,0); // 23:59
                    if (absen.StartTime.Hour <= checkout.Value.Hour && checkout.Value.Hour <= midnight.Hour) // between starttime and midnight, early checkout
                    {
                        absen.CheckOutTime = absen.StartTime.Date + new TimeSpan(checkout.Value.Hour, checkout.Value.Minute, 0); //checkout the same day before midnite
                    }
                    else
                    {
                        absen.CheckOutTime = checkout.Value;
                    }
                }
                else
                {
                    absen.CheckOutTime = checkout.Value;
                }
                absen.CheckOutLocation = comboBoxEdit2.EditValue as Data.Location;
            }

            absen.Save();

            Cursor = Cursors.Arrow;
        }
    }
}
