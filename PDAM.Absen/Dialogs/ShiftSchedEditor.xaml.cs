﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ShiftSchedEditor.xaml
    /// </summary>
    public partial class ShiftSchedEditor : Window
    {
        private XPCollection collection ;

        public ShiftSchedEditor(XPCollection collection)
        {
            InitializeComponent();

            this.collection = collection;
            DataContext = new Data.ShiftSchedule();

            tStartTime.EditValue = new DateTime(9999, 12, 1, 7, 0, 0);
        }

        public ShiftSchedEditor(Data.ShiftSchedule data)
        {
            InitializeComponent();
            DataContext = data;

            tStartTime.EditValue = new DateTime(9999, 12, 1, 7, 0, 0);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;

            var data = DataContext as Data.ShiftSchedule;
            if (this.collection != null) collection.Add(data);
            data.Save();

            Normalize();

            Cursor = Cursors.Arrow;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            AddTimeShift((tStartTime.EditValue as DateTime?).Value, int.Parse(tLength.Text), tKet.Text);
            //AddTimeShift(DateTime.Parse(tStartTime.Text), int.Parse(tLength.Text), tKet.Text);
        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            AddTimeShift(new DateTime(9999, 12, 1, 6, 0, 0), 24, "Day Off", true);
            //AddTimeShift(DateTime.Parse("12/1/9999 06:00 AM"), 24, "Day Off", true);
        }

        private void AddTimeShift(DateTime startTime, int timeLength, string ket = "", bool dayOff = false)
        {
            var data = DataContext as Data.ShiftSchedule;
            data.TimeShifts.Add(
                    new Data.TimeShift()
                    {
                        DayOrder = data.TimeShifts.Count + 1,
                        StartTime = startTime,
                        TimeLength = timeLength,
                        Ket = ket,
                        IsDayOff = dayOff
                    }
                );
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count != 1) return;

            var row = tableView1.SelectedRows[0] as Data.TimeShift;
            (DataContext as Data.ShiftSchedule).TimeShifts.Remove(row);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count != 1) return;

            var idx = tableView1.GetSelectedRowHandles()[0];
            var rowCurrent = tableView1.SelectedRows[0] as Data.TimeShift;
            var rowAbove = dataGrid.GetRow(idx - 1) as Data.TimeShift;
            
            if (rowAbove == null) return;

            SwapOrder(rowCurrent, rowAbove);
        }

        private void SwapOrder(Data.TimeShift row1, Data.TimeShift row2)
        {
            var tmp = row1.DayOrder;
            row1.DayOrder = row2.DayOrder;
            row2.DayOrder = tmp;

            row1.Save();
            row2.Save();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count != 1) return;

            var idx = tableView1.GetSelectedRowHandles()[0];
            var rowCurrent = tableView1.SelectedRows[0] as Data.TimeShift;
            var rowBelow = dataGrid.GetRow(idx + 1) as Data.TimeShift;

            if (rowBelow == null) return;

            SwapOrder(rowCurrent, rowBelow);
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            Normalize();
        }

        private void Normalize()
        {
            var col = (DataContext as Data.ShiftSchedule).TimeShifts;
            if (col.Count < 1) return;

            col.Sorting.Add(new SortProperty("DayOrder", DevExpress.Xpo.DB.SortingDirection.Ascending));

            int i = 1;
            foreach (Data.TimeShift row in col)
            {
                row.DayOrder = i++;
                row.Save();
            }
        }
    }
}
