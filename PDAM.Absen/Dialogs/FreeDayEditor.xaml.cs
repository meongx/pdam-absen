﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for FreeDayEditor.xaml
    /// </summary>
    public partial class FreeDayEditor : Window
    {
        private DevExpress.Xpo.XPCollection collection;
        private Data.FreeDay data;

        public FreeDayEditor(Data.FreeDay editData)
        {
            InitializeComponent();

            data = editData;

            if (data.Year == 0) checkBox3.IsChecked = true;
            checkBox1.IsChecked = data.AffectsRegular;
            checkBox2.IsChecked = data.AffectsShift;
            textEdit1.Text = data.Ket;
            dateEdit1.EditValue = new DateTime((data.Year == 0 ? DateTime.Now.Year : data.Year), data.Month, data.Day);
        }

        public FreeDayEditor(DevExpress.Xpo.XPCollection collection)
        {
            InitializeComponent();

            this.collection = collection;
            data = new Data.FreeDay();
            dateEdit1.EditValue = DateTime.Now;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var date = dateEdit1.EditValue as DateTime?;
            if (date == null)
            {
                MessageBox.Show("Silahkan pilih tanggal");
                return;
            }

            data.Ket = textEdit1.Text;
            data.Year = (checkBox3.IsChecked.Value ? 0 : date.Value.Year);
            data.Month = date.Value.Month;
            data.Day = date.Value.Day;
            data.AffectsRegular = checkBox1.IsChecked.Value;
            data.AffectsShift = checkBox2.IsChecked.Value;

            if (collection != null) collection.Add(data);
            data.Save();

            Close();
        }
    }
}
