﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for EditAssignment.xaml
    /// </summary>
    public partial class EditAssignment : Window
    {
        private XPCollection dataShiftSchedule;
        private XPCollection dataRegularSchedule;

        public EditAssignment(Data.Pegawai pegawai)
        {
            InitializeComponent();
            DataContext = pegawai;

            if (pegawai.RegularSchedule != null)
                radioButton2.IsChecked = true;
            else
                PrepareShiftSchedule();
        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                PrepareShiftSchedule();
        }

        private void PrepareShiftSchedule()
        {
            if (dataShiftSchedule == null) dataShiftSchedule = new XPCollection(typeof(Data.ShiftSchedule));
            comboBox1.ItemsSource = dataShiftSchedule;

            var peg = (DataContext as Data.Pegawai);
            if (peg.ShiftSchedule != null)
            {
                comboBox1.SelectedItem = peg.ShiftSchedule;
                comboBox2.SelectedIndex = peg.NextDayOrder;
            }

            layoutItem2.Visibility = System.Windows.Visibility.Visible;
        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = comboBox1.SelectedItem as Data.ShiftSchedule;
            if (selected == null || selected.TimeShifts.Sorting.Count > 0) return;

            selected.TimeShifts.Sorting.Add(new SortProperty("DayOrder", DevExpress.Xpo.DB.SortingDirection.Ascending));
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if ((comboBox1.SelectedItem == null ||
                (radioButton1.IsChecked.Value && comboBox2.SelectedItem == null))
                &&
                !radioButton3.IsChecked.Value)
            {
                MessageBox.Show("Silahkan pilih jenis Jadwal dan Next Shift (untuk jadwal Shift)", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                var peg = DataContext as Data.Pegawai;
                var link = peg.Link;

                if (link == null)
                    link = new Data.PegawaiLink() { Link = peg.Nip };

                if (radioButton1.IsChecked.Value)
                {
                    link.RegularSchedule = peg.RegularSchedule = null;
                    link.ShiftSchedule = peg.ShiftSchedule = comboBox1.SelectedItem as Data.ShiftSchedule;
                    link.NextDayOrder = peg.NextDayOrder = (comboBox2.SelectedItem as Data.TimeShift).DayOrder - 1;
                    link.Save();
                }
                else if (radioButton2.IsChecked.Value)
                {
                    link.ShiftSchedule = peg.ShiftSchedule = null;
                    link.RegularSchedule = peg.RegularSchedule = comboBox1.SelectedItem as Data.RegularSchedule;
                    link.Save();
                }
                else
                {
                    /*peg.Link = null;
                    link.Delete();
                    link = null;*/
                    //peg.Save();
                    link.ShiftSchedule = null;
                    link.RegularSchedule = null;
                    peg.Link = null;
                    link.Save();
                }

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void radioButton2_Checked(object sender, RoutedEventArgs e)
        {
            if (dataRegularSchedule == null) dataRegularSchedule = new XPCollection(typeof(Data.RegularSchedule));
            comboBox1.ItemsSource = dataRegularSchedule;

            var peg = (DataContext as Data.Pegawai);
            if (peg.RegularSchedule != null)
            {
                comboBox1.SelectedItem = peg.RegularSchedule;
            }

            layoutItem2.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void radioButton3_Checked(object sender, RoutedEventArgs e)
        {
            layoutItem1.Visibility = System.Windows.Visibility.Hidden;
            layoutItem2.Visibility = System.Windows.Visibility.Hidden;
        }

    }
}
