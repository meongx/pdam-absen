﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ApplyAbsen.xaml
    /// </summary>
    public partial class ApplyAbsen : Window
    {
        private BackgroundWorker worker = new BackgroundWorker();

        private enum DetermineTimeResult
        {
            CloserToFirst,
            CloserToSecond
        }

        public ApplyAbsen()
        {
            InitializeComponent();

            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            ResetStat();
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.IsEnabled = true;
            button1.Content = "Start";
            ResetStat();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var processed = Convert.ToInt32(e.UserState);
            
            progressBar1.Value = processed;
            if (processed == progressBar1.Maximum)
            {
                tStatus.Text = "Cleaning Up...";
                button1.IsEnabled = false;
            }
            else
                tStatus.Text = "Memproses " + processed + " dari " + progressBar1.Maximum;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int processed = 0;
            var tick = DateTime.Now;
            var deleteLog = new List<Data.RawLog>();

            var qAbsen = new XPQuery<Data.Absen>(XpoDefault.Session);
            var qPeg = new XPQuery<Data.PegawaiView>(XpoDefault.Session);
            //var maxDate = (from a in qAbsen select a.EndTime).Max();
            var minDate = (from a in qAbsen select a.StartTime).Min();

            using (var transact = new UnitOfWork())
            {
                var logs = new XPCollection<Data.RawLog>(transact);
                logs.Sorting.Add(new SortProperty("Nip", DevExpress.Xpo.DB.SortingDirection.Ascending));
                logs.Sorting.Add(new SortProperty("Time", DevExpress.Xpo.DB.SortingDirection.Ascending));
                logs.Load();

                //var absens = new XPCollection<Data.Absen>(transact,CriteriaOperator.Parse("NOT [IsDayOff]"));
                var absens = new XPCollection<Data.Absen>(transact);
                absens.Load();

                var qLoc = new XPQuery<Data.Location>(transact);

                foreach (var log in logs)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    //var location = transact.GetObjectByKey<Data.Location>(log.Location);
                    var location = qLoc.FirstOrDefault(l => l.Code == log.Location);
                    if (location == null)
                    {
                        location = new Data.Location()
                        {
                            Code = log.Location,
                            Name = "Unknown"
                        };
                        location.Save();
                    }

                    // TODO: Consider selecting only null checkin/checkout
                    var absen = absens.Where(a => a.Pegawai.Link == log.Nip && (a.StartTime.Date == log.Time.Date || a.EndTime.Date == log.Time.Date) && !a.IsDayOff).ToArray();

                    if (absen.Length == 1)
                    {
                        if (WhichIsCloser(absen[0].StartTime, absen[0].EndTime, log.Time) == DetermineTimeResult.CloserToFirst) // mostlikely checkin
                        {
                            //if ((absen[0].StartTime - log.Time).Duration().TotalHours <= 8)
                                ApplyCheckIn(absen[0], log.Time, location, logs);
                        }
                        else // mostlikely checkout
                        {
                            ApplyCheckOut(absen[0], log.Time, location, logs);
                        }
                    }
                    else if (absen.Length == 2)
                    {
                        //if (WhichIsCloser(absen[0].EndTime, absen[1].StartTime, log.Time) == DetermineTimeResult.CloserToFirst || absen[1].IsDayOff) // mostlikely checkout of first entry
                        if (WhichIsCloser(absen[0].EndTime, absen[1].StartTime, log.Time) == DetermineTimeResult.CloserToFirst) // mostlikely checkout of first entry
                        {
                            ApplyCheckOut(absen[0], log.Time, location, logs);
                        }
                        else // mostlikely checkin of second entry
                        {
                            ApplyCheckIn(absen[1], log.Time, location, logs);
                        }
                    }

                    //if (absen.Length > 0 || log.Time < maxDate) //required for non null checkin/checkout
                    if (absen.Length > 0 || log.Time < minDate)
                        deleteLog.Add(log);
                    else //if (absen.Length == 0)
                    {
                        // pegawai not exists
                        if (qPeg.Count(p => p.Nip == log.Nip) < 1)
                            deleteLog.Add(log);
                    }

                    processed++;
                    if ((DateTime.Now - tick).TotalMilliseconds >= 500)
                    {
                        worker.ReportProgress(0, processed);
                        transact.CommitChanges();
                        tick = DateTime.Now;
                    }
                }

                worker.ReportProgress(0, processed);
                foreach (var log in deleteLog)
                    log.Delete();
                transact.CommitChanges();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (worker.IsBusy)
            {
                button1.Content = "Stopping...";
                button1.IsEnabled = false;
                worker.CancelAsync();
            }
            else
            {
                tStatus.Text = "Initializing...";
                button1.Content = "Stop";
                worker.RunWorkerAsync();
            }
        }

        private void ResetStat()
        {
            var total = CountLogs();
            tStatus.Text = "Total Log belum diproses: " + total;
            progressBar1.Maximum = total;
            progressBar1.Value = 0;
        }

        private int CountLogs()
        {
            return Convert.ToInt32(XpoDefault.Session.Evaluate(typeof(Data.RawLog),CriteriaOperator.Parse("COUNT()"),null));
        }

        private void ApplyCheckIn(Data.Absen absen, DateTime time, Data.Location location, XPCollection<Data.RawLog> collection = null)
        {
            if (absen.CheckInTime.Year == 1 || absen.CheckInTime == null) // checkin not set
            {
                absen.CheckInTime = time;
                absen.CheckInLocation = location;
            }
            else // checkin is set
            {
                var count = collection.Count(log => 
                    log.Nip == absen.Pegawai.Link 
                    && log.Time.Date == time.Date 
                    && log.Time > time);
                if (count == 0 && (time - absen.CheckInTime).TotalMinutes >= 30) // no later time, means early checkout
                {
                    absen.CheckOutTime = time;
                    absen.CheckOutLocation = location;
                }
            }
        }

        private void ApplyCheckOut(Data.Absen absen, DateTime time, Data.Location location, XPCollection<Data.RawLog> collection = null)
        {
            if (absen.CheckInTime.Year == 1 || absen.CheckInTime == null) // checkin not set
            {
                var count = collection.Count(log => 
                    log.Nip == absen.Pegawai.Link 
                    && log.Time.Date == time.Date 
                    && log.Time > time 
                    && (log.Time - time).TotalHours >= 1);
                if (count > 0) // later time exists, late checkin
                {
                    absen.CheckInTime = time;
                    absen.CheckInLocation = location;
                }
                else // no later time. no checkin data?
                {
                    absen.CheckOutTime = time;
                    absen.CheckOutLocation = location;
                }
            }
            else if ((absen.CheckOutTime.Year == 1 || absen.CheckOutTime == null) && (time - absen.CheckInTime).TotalHours >= 1) // checkout not set
            {
                absen.CheckOutTime = time;
                absen.CheckOutLocation = location;
            }
        }

        private DetermineTimeResult WhichIsCloser(DateTime firstTime, DateTime secondTime, DateTime time)
        {
            var t1 = (time - firstTime).Duration();
            var t2 = (time - secondTime).Duration();

            if (t1 < t2)
            {
                return DetermineTimeResult.CloserToFirst;
            }

            return DetermineTimeResult.CloserToSecond;
        }
    }
}
