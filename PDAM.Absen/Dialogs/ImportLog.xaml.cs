﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.IO;
using System.ComponentModel;
using DevExpress.Xpo;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for ImportLog.xaml
    /// </summary>
    public partial class ImportLog : Window
    {
        private string filePath;

        private BackgroundWorker worker = new BackgroundWorker();

        public ImportLog()
        {
            InitializeComponent();

            var s = Properties.Settings.Default.InputLogFileName;
            if (!String.IsNullOrEmpty(s))
            {
                filePath = s;
                tLogFileName.Text = Path.GetFileName(filePath);
                tStatus.Text = "File Log:";
                button1.IsEnabled = true;
            }

            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Cursor = Cursors.Arrow;
            //button1.IsEnabled = true;
            tStatus.Text = "Proses Selesai";
            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = 1;

            Properties.Settings.Default.InputLogFileName = filePath;
            Properties.Settings.Default.Save();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressBar1.Maximum == 1)
            {
                progressBar1.Maximum = Convert.ToDouble(e.UserState);
                tStatus.Text = "Total Baris: " + progressBar1.Maximum;
            }
            else
            {
                progressBar1.Value = Convert.ToDouble(e.UserState);
                tStatus.Text = "Memproses Baris " + progressBar1.Value + " dari " + progressBar1.Maximum;
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var logs = File.ReadAllLines(filePath);

            if (logs.Length == 0) return;

            worker.ReportProgress(0, logs.Length);

            using (var transact = new UnitOfWork())
            {
                int processed = 0;
                var tick = DateTime.Now;

                foreach (string line in logs)
                {
                    var dt = new DateTime(
                        int.Parse(line.Substring(0, 4)),
                        int.Parse(line.Substring(4, 2)),
                        int.Parse(line.Substring(6, 2)),
                        int.Parse(line.Substring(8, 2)),
                        int.Parse(line.Substring(10, 2)),
                        0
                    );
                    var nip = int.Parse(line.Substring(12, 4));
                    var loc = int.Parse(line.Substring(16, 4));

                    var log = new Data.RawLog(transact)
                    {
                        Time = dt,
                        //Nip = String.Format("{0:000}", nip),
                        Nip = nip.ToString("000"),
                        Location = loc
                    };

                    processed++;
                    if ((DateTime.Now - tick).TotalMilliseconds >= 100)
                    {
                        worker.ReportProgress(0, processed);
                        transact.CommitChanges();
                        tick = DateTime.Now;
                    }
                }
                transact.CommitChanges();
                worker.ReportProgress(0, processed);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(filePath))
            {
                MessageBox.Show("File tidak ditemukan");
                return;
            }

            //ResetProgressBar();
            button1.IsEnabled = false;
            Cursor = Cursors.Wait;

            worker.RunWorkerAsync();
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                filePath = (e.Data.GetData(DataFormats.FileDrop, true) as string[])[0];
                tLogFileName.Text = Path.GetFileName(filePath);
                tStatus.Text = "File Log:";
                button1.IsEnabled = true;
            }
        }

    }
}
