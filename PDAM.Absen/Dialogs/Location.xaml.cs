﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpo;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for Location.xaml
    /// </summary>
    public partial class Location : Window
    {
        private XPCollection locations = new XPCollection(typeof(Data.Location)) { DeleteObjectOnRemove = true };

        public Location()
        {
            InitializeComponent();

            listLocation.ItemsSource = locations;
            DataContext = new Data.Location();
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var data = DataContext as Data.Location;
                data.Code = Convert.ToInt32(tCode.EditValue);
                data.Name = tName.Text;
                data.Save();

                DataContext = new Data.Location();
                tCode.Focus();

                if (bSave.Content as String == "Edit")
                {
                    bSave.Content = "New";
                    bCancel.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    locations.Add(data);
                }
            }
            catch (DevExpress.Xpo.DB.Exceptions.ConstraintViolationException)
            {
                MessageBox.Show("ERROR: Duplicate Code");
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            EditItem();
        }

        private void listLocation_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItem();
        }

        private void EditItem()
        {
            if (listLocation.SelectedItem == null) return;
            
            DataContext = listLocation.SelectedItem;

            bSave.Content = "Edit";
            bCancel.Visibility = System.Windows.Visibility.Visible;
            tName.Focus();
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new Data.Location();

            bSave.Content = "New";
            bCancel.Visibility = System.Windows.Visibility.Collapsed;
            tCode.Focus();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (listLocation.SelectedItem == null) return;

            try
            {
                var data = listLocation.SelectedItem as Data.Location;
                //data.Delete();
                locations.Remove(data);
                //locations.Reload();
            }
            catch
            {
                MessageBox.Show("Gagal menghapus: Ada data absen yang terhubung ke lokasi");
            }
        }
    }
}
