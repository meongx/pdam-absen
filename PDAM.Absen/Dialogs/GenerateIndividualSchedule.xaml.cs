﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Dialogs
{
    /// <summary>
    /// Interaction logic for GenerateIndividualSchedule.xaml
    /// </summary>
    public partial class GenerateIndividualSchedule : Window
    {
        private Data.Pegawai pegawai;

        public GenerateIndividualSchedule(Data.Pegawai pegawai)
        {
            InitializeComponent();

            this.pegawai = pegawai;
            DataContext = this.pegawai;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (dateEdit1.EditValue == null || dateEdit2.EditValue == null) return;

            Cursor = Cursors.Wait;
            button1.IsEnabled = false;

            var StartDate = (dateEdit1.EditValue as DateTime?).Value;
            var EndDate = (dateEdit2.EditValue as DateTime?).Value;
            var freeDays = new XPCollection<Data.FreeDay>();
            var qExists = new XPQuery<Data.Absen>(XpoDefault.Session);

            if (pegawai.ShiftSchedule != null)
            {
                XPCollection<Data.TimeShift> shifts = pegawai.ShiftSchedule.TimeShifts;
                shifts.Sorting.Add(new SortProperty("DayOrder", DevExpress.Xpo.DB.SortingDirection.Ascending));
                shifts.Load();

                for (DateTime tgl = StartDate.Date; tgl <= EndDate.Date; tgl = tgl.AddDays(1))
                {
                    var nextShift = shifts.First(s => s.DayOrder == pegawai.NextDayOrder + 1);

                    freeDays.Filter = CriteriaOperator.Parse(
                        "[AffectsShift] == true And ([Month] == ? And [Day] == ? And ([Year] == 0 Or [Year] == ?))",
                        tgl.Month, tgl.Day, tgl.Year
                    );

                    var absen = qExists.FirstOrDefault(a => a.StartTime.Date == tgl.Date && a.Pegawai == pegawai.Link);
                    if (absen == null) absen = new Data.Absen();

                    absen.StartTime = tgl.Add(new TimeSpan(nextShift.StartTime.Hour, nextShift.StartTime.Minute, 0));
                    absen.TimeLength = nextShift.TimeLength;
                    absen.Ket = (freeDays.Count == 0 ? nextShift.Ket : freeDays[0].Ket);
                    absen.DayOrder = pegawai.NextDayOrder;
                    absen.IsDayOff = (freeDays.Count == 0 ? nextShift.IsDayOff : true);

                    pegawai.Link.Absens.Add(absen);
                    absen.Save();

                    if (++pegawai.NextDayOrder >= shifts.Count) pegawai.NextDayOrder = 0;
                }
            }
            else if (pegawai.RegularSchedule != null)
            {
                XPCollection<Data.TimeRegular> shifts = pegawai.RegularSchedule.TimeRegulars;

                for (DateTime tgl = StartDate.Date; tgl <= EndDate.Date; tgl = tgl.AddDays(1))
                {
                    freeDays.Filter = CriteriaOperator.Parse(
                        "[AffectsRegular] == true And ([Month] == ? And [Day] == ? And ([Year] == 0 Or [Year] == ?))",
                        tgl.Month, tgl.Day, tgl.Year
                    );

                    var day = shifts.First(s =>
                    {
                        if (s.DayOfWeek == Data.DayOfWeek.Senin && tgl.DayOfWeek == DayOfWeek.Monday) return true;
                        if (s.DayOfWeek == Data.DayOfWeek.Minggu && tgl.DayOfWeek == DayOfWeek.Sunday) return true;
                        if (Convert.ToByte(s.DayOfWeek) == Convert.ToByte(tgl.DayOfWeek) - 1) return true;

                        return false;
                    });

                    var absen = qExists.FirstOrDefault(a => a.StartTime.Date == tgl.Date && a.Pegawai == pegawai.Link);
                    if (absen == null) absen = new Data.Absen();

                    absen.StartTime = tgl.Add(new TimeSpan(day.StartTime.Hour, day.StartTime.Minute, 0));
                    absen.TimeLength = day.TimeLength;
                    absen.Ket = (freeDays.Count == 0 ? day.Ket : freeDays[0].Ket);
                    absen.IsDayOff = (freeDays.Count == 0 ? day.IsDayOff : true);

                    pegawai.Link.Absens.Add(absen);
                    absen.Save();
                }
            }
            //pegawai.Save();

            Cursor = Cursors.Arrow;
            button1.IsEnabled = true;
        }

    }
}
