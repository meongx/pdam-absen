﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PDAM.Absen.Pages
{
    /// <summary>
    /// Interaction logic for ShiftSched.xaml
    /// </summary>
    public partial class ShiftSched : UserControl
    {
        public ShiftSched(DevExpress.Xpo.XPCollection data)
        {
            InitializeComponent();

            data.DeleteObjectOnRemove = true;
            listSched.ItemsSource = data;
        }

        private void chartControl1_CustomDrawSeriesPoint(object sender, DevExpress.Xpf.Charts.CustomDrawSeriesPointEventArgs e)
        {
            var data = (e.SeriesPoint.Tag as Data.TimeShift);
            e.LabelText = data.Ket;

            if (data.IsDayOff)
                e.DrawOptions.Color = Color.FromRgb(0xEA, 0x0B, 0x0B);
        }

        private void listSched_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listSched.SelectedItem == null) return;

            Cursor = Cursors.Wait;
            new Dialogs.ShiftSchedEditor(listSched.SelectedItem as Data.ShiftSchedule).Show();
            Cursor = Cursors.Arrow;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            (listSched.ItemsSource as DevExpress.Xpo.XPCollection).Reload();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            listSched_MouseDoubleClick(null, null);
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (listSched.SelectedItem == null) return;

            var data = listSched.SelectedItem as Data.ShiftSchedule;
            if (MessageBox.Show("Yakin Hapus Jadwal " + data.Name,"DELETE",MessageBoxButton.OKCancel,MessageBoxImage.Exclamation) == MessageBoxResult.OK)
                (listSched.ItemsSource as DevExpress.Xpo.XPCollection).Remove(data);
        }
    }
}
