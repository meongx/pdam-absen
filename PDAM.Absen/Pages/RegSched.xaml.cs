﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpo;

namespace PDAM.Absen.Pages
{
    /// <summary>
    /// Interaction logic for RegSched.xaml
    /// </summary>
    public partial class RegSched : UserControl
    {
        public RegSched(XPCollection data)
        {
            InitializeComponent();
            data.DeleteObjectOnRemove = true;
            listBox1.ItemsSource = data;
        }

        private void listBox1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listBox1.SelectedItem == null) return;

            Cursor = Cursors.Wait;
            new Dialogs.RegularSchedEditor(listBox1.SelectedItem as Data.RegularSchedule).Show();
            Cursor = Cursors.Arrow;
        }

        private void chartControl1_CustomDrawSeriesPoint(object sender, DevExpress.Xpf.Charts.CustomDrawSeriesPointEventArgs e)
        {
            var data = (e.SeriesPoint.Tag as Data.TimeRegular);
            e.LabelText = data.Ket;

            if (data.IsDayOff)
                e.DrawOptions.Color = Color.FromRgb(0xEA, 0x0B, 0x0B);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            (listBox1.ItemsSource as XPCollection).Reload();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            listBox1_MouseDoubleClick(null, null);
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (listBox1.SelectedItem == null) return;

            var data = listBox1.SelectedItem as Data.RegularSchedule;
            if (MessageBox.Show("Yakin Hapus Jadwal " + data.Name, "DELETE", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK)
                (listBox1.ItemsSource as DevExpress.Xpo.XPCollection).Remove(data);
        }
    }
}
