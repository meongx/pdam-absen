﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpo;
using DevExpress.Data;
using DevExpress.Data.Filtering;

namespace PDAM.Absen.Pages
{
    /// <summary>
    /// Interaction logic for Pegawai.xaml
    /// </summary>
    public partial class Pegawai : UserControl
    {
        //private XPServerCollectionSource dataSource = new XPServerCollectionSource(XpoDefault.Session,typeof(Data.PegawaiView));
        //private XPCollection dataSource = new XPCollection(typeof(Data.PegawaiLink));

        public Pegawai()
        {
            InitializeComponent();
            gridControl1.ItemsSource = Data.Pegawai.GetDataView();

            var now = DateTime.Today;
            if (now.Day < 16)
            {
                dateEdit1.EditValue = new DateTime((now.Month == 1 ? now.Year - 1 : now.Year), (now.Month == 1 ? 12 : now.Month - 1), 16);
                dateEdit2.EditValue = new DateTime(now.Year, now.Month, 15);
            }
            else
            {
                dateEdit1.EditValue = new DateTime(now.Year, now.Month, 16);
                dateEdit2.EditValue = new DateTime((now.Month == 12 ? now.Year + 1 : now.Year), (now.Month == 12 ? 1 : now.Month + 1), 15);
            }
            DisplayAbsen();
        }

        public Data.Pegawai GetSelected()
        {
            if (tableView1.SelectedRows.Count == 0) return null;
            return tableView1.SelectedRows[0] as Data.Pegawai;
        }

        private void tableView1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (tableView1.GetRowHandleByMouseEventArgs(e) != GridDataController.InvalidRow)
                EditAssignment();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            gridControl1.ItemsSource = Data.Pegawai.GetDataView();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            EditAssignment();
        }

        private void EditAssignment()
        {
            if (tableView1.SelectedRows.Count < 1) return;

            new Dialogs.EditAssignment(tableView1.SelectedRows[0] as Data.Pegawai).Show();
        }

        private void tableView1_SelectionChanged(object sender, DevExpress.Xpf.Grid.GridSelectionChangedEventArgs e)
        {
            if (!IsLoaded) return;

            DisplayAbsen();
        }

        private void DisplayAbsen()
        {
            if (tableView1.SelectedRows.Count == 0) return;

            var date1 = (dateEdit1.EditValue as DateTime?);
            var date2 = (dateEdit2.EditValue as DateTime?);

            var peg = tableView1.SelectedRows[0] as Data.Pegawai;
            var link = XpoDefault.Session.GetObjectByKey<Data.PegawaiLink>(peg.Oid);

            if (tableView1.SelectedRows.Count == 0 || date1 == null || date2 == null || link == null)
            {
                chartSeries1.DataSource = null;
                return;
            }

            link.Absens.Filter = CriteriaOperator.Parse(
                "GetDate(StartTime) >= ? And GetDate(StartTime) <= ?",
                date1.Value.Date,
                date2.Value.Date
            );
            link.Absens.Reload();

            chartSeries1.DataSource = link.Absens;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var date1 = (dateEdit1.EditValue as DateTime?);
            var date2 = (dateEdit2.EditValue as DateTime?);

            var col = chartSeries1.DataSource as XPCollection<Data.Absen>;
            if (col == null || date1 == null || date2 == null) return;

            chartSeries1.DataSource = null;
            col.Filter = CriteriaOperator.Parse(
                "GetDate(StartTime) >= ? And GetDate(StartTime) <= ?",
                date1.Value.Date,
                date2.Value.Date
            );
            col.Reload();
            chartSeries1.DataSource = col;
        }

        private void chartControl1_CustomDrawSeriesPoint(object sender, DevExpress.Xpf.Charts.CustomDrawSeriesPointEventArgs e)
        {
            var data = (e.SeriesPoint.Tag as Data.Absen);
            e.LabelText = data.Ket;

            if (data.IsDayOff)
                e.DrawOptions.Color = Color.FromRgb(0xEA, 0x0B, 0x0B);
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count < 1) return;

            new Dialogs.GenerateIndividualSchedule(tableView1.SelectedRows[0] as Data.Pegawai).Show();
        }
    }
}
