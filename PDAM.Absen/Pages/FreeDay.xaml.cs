﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PDAM.Absen.Pages
{
    /// <summary>
    /// Interaction logic for FreeDay.xaml
    /// </summary>
    public partial class FreeDay : UserControl
    {
        public FreeDay(DevExpress.Xpo.XPCollection collection)
        {
            InitializeComponent();

            collection.DeleteObjectOnRemove = true;
            gridControl1.ItemsSource = collection;
            gridControl1.GroupBy("Year");
            gridControl1.GroupBy("DisplayMonth");
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            new Dialogs.FreeDayEditor(gridControl1.ItemsSource as DevExpress.Xpo.XPCollection).Show();
        }

        private void tableView1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowhandle = tableView1.GetRowHandleByMouseEventArgs(e);
            if (rowhandle < 0 || rowhandle == DevExpress.Data.GridDataController.InvalidRow) return;

            new Dialogs.FreeDayEditor(gridControl1.GetRow(rowhandle) as Data.FreeDay).Show();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (tableView1.SelectedRows.Count < 1) return;

            var confirm = MessageBox.Show(
                    "Menghapus hari libur tidak merubah jadwal yang telah di-Generate\nYakin hapus hari libur?",
                    "",
                    MessageBoxButton.OKCancel,
                    MessageBoxImage.Warning
                );
            if (confirm == MessageBoxResult.OK)
            {
                (tableView1.SelectedRows[0] as Data.FreeDay).Delete();
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            (gridControl1.ItemsSource as DevExpress.Xpo.XPCollection).Reload();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count < 1) return;

            new Dialogs.FreeDayEditor(tableView1.SelectedRows[0] as Data.FreeDay).Show();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (tableView1.SelectedRows.Count < 1) return;

            var data = tableView1.SelectedRows[0] as Data.FreeDay;
            if (MessageBox.Show("Yakin Hapus Hari Libur " + data.Ket, "DELETE", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK)
                (gridControl1.ItemsSource as DevExpress.Xpo.XPCollection).Remove(data);
        }
    }
}
